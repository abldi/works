
    $("#hide").click(function() {
        if($(this).hasClass("clicked")){
            $(this).removeClass("clicked");
        
            $("#ControlPanel").animate({bottom: "-190px"});
            $(this).addClass("bottom10");
            $(this).removeClass("bottom70");
            // $("#hide").animate({bottom: "10px"});
            $("#hide").css('background-image', 'url(textures/icons/up.png)');
        }else{
            $(this).addClass("clicked");
        
            $("#ControlPanel").animate({bottom: "10px"});
            $(this).addClass("bottom70");
            $(this).removeClass("bottom10");
            $("#hide").css('background-image', 'url(textures/icons/down.png)');
        }
        
    });
