/**
 * Created by zhou-fu on 17-10-16.
 */

function DefaultPanoramaLoad() {

    this.load = function ( data, scene, geometry ) {

        if (!data){
            console.warn("Panorama data is null");
            return null;
        }

        console.log(data.type)
        switch (data.type){
            case 'equirectangular':{
                let material_ = new THREE.MeshBasicMaterial({color: 0xffffff});
                let material_s, material_l;

                let mesh = new THREE.Mesh( geometry, material_ );
                scene.add( mesh );

                material_s = new THREE.MeshBasicMaterial( {
                    map: new THREE.TextureLoader().load( data.panorama.small, function(texture){
                        if (material_s && mesh.material.uuid === material_.uuid){
                            mesh.material = material_s;
                        }
                    })
                } );

                material_l = new THREE.MeshBasicMaterial( {
                    map: new THREE.TextureLoader().load( data.panorama.large, function(texture){
                        if (material_l){
                            mesh.material = material_l;
                        }
                    })
                } );

                console.log(material_l)

                break;
            }

            case 'cube':{

                let loader = new THREE.CubeTextureLoader();

                let textureCube_s = loader.load( [
                    data.panorama.px_s, data.panorama.nx_s,
                    data.panorama.py_s, data.panorama.ny_s,
                    data.panorama.pz_s, data.panorama.nz_s
                ], function(object){
                    if (!scene.background){
                        scene.background = textureCube_s;
                    }
                } );

                let textureCube = loader.load( [
                    data.panorama.px, data.panorama.nx,
                    data.panorama.py, data.panorama.ny,
                    data.panorama.pz, data.panorama.nz
                ], function(object){
                    scene.background = textureCube;
                });
            }

        }
    };
}

let PanoramaLoad = new DefaultPanoramaLoad();