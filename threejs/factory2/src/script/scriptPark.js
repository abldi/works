// var group;
// var cameraAnimation = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, .1, 2000 );
// var pipeSpline = [
//         new THREE.Vector3( 310, 80, 210 ),
//         new THREE.Vector3( 270, 80, 130 ),
//         new THREE.Vector3( 250, 10, -14 ),
//         new THREE.Vector3( 180, 2, -30 ),
//         new THREE.Vector3( 100, 2, -30 ),
//         new THREE.Vector3( 30, 2, -30 ),
//         new THREE.Vector3( -40, 2, -30 ),
//         new THREE.Vector3( -70, 2, -30 ),
//         new THREE.Vector3( -90, 2, -30 ),
//         new THREE.Vector3( -160, 2, -30 ),
//         new THREE.Vector3( -210, 2, -30 ),//11
//         new THREE.Vector3( -280, 10, -45 ),
//         new THREE.Vector3( -300, 40, -100 ),
//         new THREE.Vector3( -210, 60, -190 ),
//         new THREE.Vector3( -50, 80, -100 ),
//         new THREE.Vector3( -30, 80, -20 ),
//         new THREE.Vector3( -25, 60, 50 ),
//         new THREE.Vector3( -5, 60, 120 ),
//         new THREE.Vector3( 90, 60, 190 ),
//         new THREE.Vector3( 150, 70, 180 ),//20
//         new THREE.Vector3( 250, 80, 210 )
//     ]

// var pipeSpline2 = [
//         new THREE.Vector3( 130, 0, 0 ),
//         new THREE.Vector3( 100, 0, -10 ),
//         new THREE.Vector3( 80, 0, -26 ),
//         new THREE.Vector3( 40, 0, -30 ),
//         new THREE.Vector3( -10, 2, -30 ),
//         new THREE.Vector3( -20, 2, 0 ),
//         new THREE.Vector3( -25, 2, 0 ),
//         new THREE.Vector3( -120, 2, -10 ),
//         new THREE.Vector3( -200, 2, -30 ),
//         new THREE.Vector3( -240, 2, -30 ),
//         new THREE.Vector3( -280, 2, -90 ),//11
//         new THREE.Vector3( -260, 2, -120 ),
//         new THREE.Vector3( -190, 2, -140 ),
//         new THREE.Vector3( -120, 2, -100 ),
//         new THREE.Vector3( 20, 2, -10 ),
//         new THREE.Vector3( 30, 2, 50 ),
//         new THREE.Vector3( 60, 2, 90 ),
//         new THREE.Vector3( 70, 2, 100 ),
//         new THREE.Vector3( 100, 2, 110 ),
//         new THREE.Vector3( 120, 2, 105 ),
//         new THREE.Vector3( 140, 2, 50 )
//     ]

// var spline = new THREE.SplineCurve3(pipeSpline);
// var spline2 = new THREE.SplineCurve3(pipeSpline2);

var camPosIndex = 0;

var scene;
var camera;
var renderer = null;
var container = null;
var controls = null;
var clock = null;
var stats = null;




  function init() { // Initialization

    THREE.DefaultLoadingManager.onProgress = function ( item, loaded, total ) {

          $('.loadingbarcolor')[0].style.width = Math.min(100, parseInt(100.0 * loaded / total)) + '%';

          console.log(Math.min(100, parseInt(100.0 * loaded / total)));

          if (Math.min(100, parseInt(100.0 * loaded / total)) == 100) {
              $('.showload')[0].style.display = 'none';
          }

          return Math.min(100, parseInt(100.0 * loaded / total));
          //console.log( item, loaded, total );
      };

    // create main scene
    scene = new THREE.Scene();
    scene.fog = new THREE.FogExp2(0xcce0ff, 0.001);


    







    group = new THREE.Group();
       scene.add( group );

    var SCREEN_WIDTH = window.innerWidth,

        SCREEN_HEIGHT = window.innerHeight;
        console.log(SCREEN_WIDTH);
    // prepare camera
    var VIEW_ANGLE = 60, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 1, FAR = 2000;
    camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
    scene.add(camera);
    camera.position.set(200, 100, 300);
    // camera.lookAt(new THREE.Vector3(0,0,0));



    // prepare renderer
    renderer = new THREE.WebGLRenderer({ antialias:true });

    renderer.setPixelRatio(window.devicePixelRatio);

    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    renderer.setClearColor(scene.fog.color);
    renderer.shadowMapEnabled = true;
    renderer.shadowMapSoft = true;

    // prepare container
    container = document.createElement('div');
    document.body.appendChild(container);
    container.appendChild(renderer.domElement);

    // events
    THREEx.WindowResize(renderer, camera);

    // // prepare controls (OrbitControls)
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.target = new THREE.Vector3(50, 0, 100)
    controls.minDistance = 350;
    controls.maxDistance = 600;
    controls.maxPolarAngle = Math.PI/2.5; 

    controls.rotateSpeed = 0.3;

    // controls.center.set(spline2.getPoint(camPosIndex / 500));
    camera.position.copy(controls.center).add(new THREE.Vector3(200, 200, 400));

    // prepare clock
    clock = new THREE.Clock();


    scene.add(new THREE.AmbientLight(0x576095));

    var light;

    light = new THREE.DirectionalLight(0xffffff, 0.7);//e0f3f7
    light.position.set(-200, 400, 300);
    light.position.multiplyScalar(1);

    light.castShadow = true;
    light.shadowCameraVisible = true;
    light.shadow.bias = 0.0001; // use bias to tweak

    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;

    var d = 240;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;

    scene.add(light);



    var lightL;

    lightL = new THREE.DirectionalLight(0xdfebff, 0.3);
    lightL.position.set(-200, 400, -300);
    lightL.position.multiplyScalar(1);

    lightL.castShadow = false;
    lightL.shadowCameraVisible = false;
    light.shadow.bias = -0.0005; // use bias to tweak


    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;

    var d = 360;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;
    scene.add(lightL);

    light = new THREE.HemisphereLight(0xffffff, 0x444444, 1.0);
        light.position.set(0, 1, 0);
        scene.add(light);


    // A1
    var map = THREE.ImageUtils.loadTexture( "../src/textures/Parksprites/1.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 70;
    sprite.position.y = 30;
    sprite.position.z = -85;
    sprite.scale.x = 100;
    sprite.scale.y = 100;
    sprite.scale.z = 100;
    this.scene.add( sprite );
    // this.objects.push( sprite );

    // A2
    var map = THREE.ImageUtils.loadTexture( "../src/textures/Parksprites/2.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 70;
    sprite.position.y = 30;
    sprite.position.z = 0;
    sprite.scale.x = 100;
    sprite.scale.y = 100;
    sprite.scale.z = 100;
    this.scene.add( sprite );

    // A3
    var map = THREE.ImageUtils.loadTexture( "../src/textures/Parksprites/3.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 70;
    sprite.position.y = 30;
    sprite.position.z = 100;
    sprite.scale.x = 100;
    sprite.scale.y = 100;
    sprite.scale.z = 100;
    this.scene.add( sprite );

    // A4
    var map = THREE.ImageUtils.loadTexture( "../src/textures/Parksprites/4.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 70;
    sprite.position.y = 30;
    sprite.position.z = 190;
    sprite.scale.x = 100;
    sprite.scale.y = 100;
    sprite.scale.z = 100;
    this.scene.add( sprite );



    // A5
    var map = THREE.ImageUtils.loadTexture( "../src/textures/Parksprites/5.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -65;
    sprite.position.y = 30;
    sprite.position.z = 190;
    sprite.scale.x = 100;
    sprite.scale.y = 100;
    sprite.scale.z = 100;
    this.scene.add( sprite );

    // A6
    var map = THREE.ImageUtils.loadTexture( "../src/textures/Parksprites/6.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -65;
    sprite.position.y = 30;
    sprite.position.z = 100;
    sprite.scale.x = 100;
    sprite.scale.y = 100;
    sprite.scale.z = 100;
    this.scene.add( sprite );

    // A7
    var map = THREE.ImageUtils.loadTexture( "../src/textures/Parksprites/7.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -40;
    sprite.position.y = 30;
    sprite.position.z = -50;
    sprite.scale.x = 100;
    sprite.scale.y = 100;
    sprite.scale.z = 100;
    this.scene.add( sprite );

    // add simple ground
    // var ground = new THREE.Mesh( new THREE.PlaneGeometry(20, 20, 10, 10), new THREE.MeshPhongMaterial({color: 0xff0000}) );
    // ground.position.set(0, 10, 0);
    // ground.rotation.x = -Math.PI / 2;
    // scene.add(ground);


      // var loader = new THREE.FBXLoader( );
      //   loader.load( '../src/models/parkbasicmodels/square/square_shell.FBX', function( object, materials ) {
      //     // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

      //     object.traverse( function(child) {
      //       if (child instanceof THREE.Mesh) {
      //         // enable casting shadows
      //         // child.material = material2;
      //         child.castShadow = true;
      //         child.receiveShadow = true;
      //         // child.material.side = THREE.DoubleSide;
      //       }
      //     });
          
      //     object.position.x = 0;
      //     object.position.y = -0.1;
      //     object.position.z = 0;

      //     object.rotation.z = 0; //Rotattion
      //     object.scale.set(0.06, 0.06, 0.06);
      //     scene.add(object);


      //   })



      var loadersky = new THREE.TextureLoader();
        loadersky.load( '../src/textures/skyboxflip.jpg', function ( texture ) {

          var geometry = new THREE.SphereGeometry( -2500, 10, 10 );


          var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 1 } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.y = Math.PI;
          group.add( mesh );

        } );


    // loadModelbuilding();
    // loadModelwall();
    // loadModelwindow();
    // loadModelH();

    // loadModelbase();
    // loadModelceiling();
    // loadModelceilingglass();
    // loadModelfloor();
    // loadModelglass();
    // loadModelstreet();
    // loadModelgreen();
    // loadModelwingwindow();
    // loadModelwingbody();
    // loadModelwingceiling();
    // loadModeloutsidefloor();
    // loadModelwingframe();
    // loadModelsidebuilding();
    // loadModelcarslot();
    // loadModelall();
    // loadModeledge();

    loadModelwater();
    loadGround();
    loadParkbrickroad();
    loadParksidebuildings();
    loadParkstreets();
    loadStreets();
    loadGrass();
    loadRoadEdge();

    loadSquare_shell();
    loadSquare_glasses_park();
    // loadSquare_shell_park();
    loadSquare_edges_park();
    loadSquare_top_park();
    loadSquare_wall_type1();
    loadSquare_wall_typeGold_park();
    loadsquare_backwall();

    // loadsquare_door();

    loadLong_edges_park();
    loadLong_frame_park();
    loadLong_glasses_park();
    loadLong_top_park();
    loadLong_wall_gold_park();
    loadLong_shell();
    // loadLong_door();
  }

///////////////////////GROUND

  function loadModelwater() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/water.obj', function(object, materials) {

    var background = new THREE.CubeTextureLoader()
          .setPath( '../src/textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0xa3b8d5, envMap: background } );
      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 2.3;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      scene.add(object);
    });
  }


  function loadGround() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/ground.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials); 5e4f3e
      var material2 = new THREE.MeshPhongMaterial( { color: 0x7c6851 } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 2.5;
      object.position.z = 50;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(1, 1, 1);
      scene.add(object);
    });
  }


  function loadGrass() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials); 5e4f3e
      var material2 = new THREE.MeshPhongMaterial({ color: 0x091600, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 2.5;
      object.position.z = 50;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(1, 1, 1);
      scene.add(object);
    });
  }

  function loadRoadEdge() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/roadedge.obj', function(object, materials) {

      /// var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x5e5955 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 3.5;
      object.position.z = 50;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(1, 1, 1);
      scene.add(object);
    });
  }


  function loadParkbrickroad() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/parkbrickroad.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x5e5955 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 3.5;
      object.position.z = 50;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(1, 1, 1);
      scene.add(object);
    });
  }




  function loadParksidebuildings() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/parksidebuildings.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({color: 0xbcbcbc, transparent: true, opacity: 0.4});

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = false;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(1, 1, 1);
      scene.add(object);
    });
  }


  function loadParkstreets() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/parkstreets.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x333333 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 3.5;
      object.position.z = 50;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(1, 1, 1);
      scene.add(object);
    });
  }


  function loadStreets() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/streets.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x333333 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 3.5;
      object.position.z = 50;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(1, 1, 1);
      scene.add(object);
    });
  }

  ///////////////////////SQUARE BUILDING

  function loadSquare_shell()  {

    // prepare loader and load the model

        var loader = new THREE.FBXLoader( );
        loader.load( '../src/models/parkbasicmodels/square/square_shell.FBX', function( object, materials ) {
          // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

          object.traverse( function(child) {
            if (child instanceof THREE.Mesh) {
              // enable casting shadows
              // child.material = material2;
              child.castShadow = true;
              child.receiveShadow = true;
              // child.material.side = THREE.DoubleSide;
            }
          });
          
          var object1 = object.clone();
          object1.position.x = 68;
          object1.position.y = 0;
          object1.position.z = -50;

          object1.rotation.z = 0; //Rotattion
          object1.scale.set(0.001, 0.001, 0.001);
          scene.add(object1);

          var object2 = object.clone();
          object2.position.x = 68;
          object2.position.y = 0;
          object2.position.z = 35;

          object2.rotation.z = 0; //Rotattion
          object2.scale.set(0.001, 0.001, 0.001);
          scene.add(object2);

          var object3 = object.clone();
          object3.position.x = 68;
          object3.position.y = 0;
          object3.position.z = 138;

          object3.rotation.z = 0; //Rotattion
          object3.scale.set(0.001, 0.001, 0.001);
          scene.add(object3);

          var object4 = object.clone();
          object4.position.x = 68;
          object4.position.y = 0;
          object4.position.z = 228;

          object4.rotation.z = 0; //Rotattion
          object4.scale.set(0.001, 0.001, 0.001);
          scene.add(object4);

          var object5 = object.clone();
          object5.position.x = -65;
          object5.position.y = 0;
          object5.position.z = 138;

          object5.rotation.z = 0; //Rotattion
          object5.scale.set(0.001, 0.001, 0.001);
          scene.add(object5);

          var object6 = object.clone();
          object6.position.x = -65;
          object6.position.y = 0;
          object6.position.z = 228;

          object6.rotation.z = 0; //Rotattion
          object6.scale.set(0.001, 0.001, 0.001);
          scene.add(object6);


        })
  }



  function loadsquare_backwall() {
        // prepare loader and load the model

        var loader = new THREE.FBXLoader( );
        loader.load( '../src/models/square/square_backwall.FBX', function( object, materials ) {
          // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

          object.traverse( function(child) {
            if (child instanceof THREE.Mesh) {
              // enable casting shadows
              // child.material = material2;
              child.castShadow = true;
              child.receiveShadow = true;
              child.material.side = THREE.DoubleSide;
            }
          });
          
          var object1 = object.clone();
          object1.position.x = 68;
          object1.position.y = 0;
          object1.position.z = -50;

          object1.rotation.z = 0; //Rotattion
          object1.scale.set(0.001, 0.001, 0.001);
          scene.add(object1);

          var object2 = object.clone();
          object2.position.x = 68;
          object2.position.y = 0;
          object2.position.z = 35;

          object2.rotation.z = 0; //Rotattion
          object2.scale.set(0.001, 0.001, 0.001);
          scene.add(object2);

          var object3 = object.clone();
          object3.position.x = 68;
          object3.position.y = 0;
          object3.position.z = 138;

          object3.rotation.z = 0; //Rotattion
          object3.scale.set(0.001, 0.001, 0.001);
          scene.add(object3);

          var object4 = object.clone();
          object4.position.x = 68;
          object4.position.y = 0;
          object4.position.z = 228;

          object4.rotation.z = 0; //Rotattion
          object4.scale.set(0.001, 0.001, 0.001);
          scene.add(object4);

          var object5 = object.clone();
          object5.position.x = -65;
          object5.position.y = 0;
          object5.position.z = 138;

          object5.rotation.z = 0; //Rotattion
          object5.scale.set(0.001, 0.001, 0.001);
          scene.add(object5);

          var object6 = object.clone();
          object6.position.x = -65;
          object6.position.y = 0;
          object6.position.z = 228;

          object6.rotation.z = 0; //Rotattion
          object6.scale.set(0.001, 0.001, 0.001);
          scene.add(object6);
        })
  }

  function loadSquare_glasses_park() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/square/square_glasses_park.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var background = new THREE.CubeTextureLoader()
          .setPath( '../src/textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshBasicMaterial( { color: 0xb9a988, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      var object1 = object.clone();
          object1.position.x = 68;
          object1.position.y = 0;
          object1.position.z = -50;

          object1.rotation.z = 0; //Rotattion
          object1.scale.set(0.001, 0.001, 0.001);
          scene.add(object1);

          var object2 = object.clone();
          object2.position.x = 68;
          object2.position.y = 0;
          object2.position.z = 35;

          object2.rotation.z = 0; //Rotattion
          object2.scale.set(0.001, 0.001, 0.001);
          scene.add(object2);

          var object3 = object.clone();
          object3.position.x = 68;
          object3.position.y = 0;
          object3.position.z = 138;

          object3.rotation.z = 0; //Rotattion
          object3.scale.set(0.001, 0.001, 0.001);
          scene.add(object3);

          var object4 = object.clone();
          object4.position.x = 68;
          object4.position.y = 0;
          object4.position.z = 228;

          object4.rotation.z = 0; //Rotattion
          object4.scale.set(0.001, 0.001, 0.001);
          scene.add(object4);

          var object5 = object.clone();
          object5.position.x = -65;
          object5.position.y = 0;
          object5.position.z = 138;

          object5.rotation.z = 0; //Rotattion
          object5.scale.set(0.001, 0.001, 0.001);
          scene.add(object5);

          var object6 = object.clone();
          object6.position.x = -65;
          object6.position.y = 0;
          object6.position.z = 228;

          object6.rotation.z = 0; //Rotattion
          object6.scale.set(0.001, 0.001, 0.001);
          scene.add(object6);
    });
  }


  // function loadSquare_shell_park() {

  //   // prepare loader and load the model
  //   var oLoader = new THREE.OBJLoader();
  //   oLoader.load('../src/models/parkbasicmodels/square/square_shell_park.obj', function(object, materials) {

  //     // var material = new THREE.MeshFaceMaterial(materials);
  //     var material2 = new THREE.MeshPhongMaterial({ color: 0x666666 });

  //     object.traverse( function(child) {
  //       if (child instanceof THREE.Mesh) {

  //         // apply custom material
  //         child.material = material2;
  //         // child.material.side = THREE.DoubleSide;
  //         // enable casting shadows
  //         child.castShadow = true;
  //         child.receiveShadow = true;
  //       }
  //     });
      
  //     var object1 = object.clone();
  //         object1.position.x = 68;
  //         object1.position.y = 0;
  //         object1.position.z = -50;

  //         object1.rotation.z = 0; //Rotattion
  //         object1.scale.set(0.001, 0.001, 0.001);
  //         scene.add(object1);

  //         var object2 = object.clone();
  //         object2.position.x = 68;
  //         object2.position.y = 0;
  //         object2.position.z = 35;

  //         object2.rotation.z = 0; //Rotattion
  //         object2.scale.set(0.001, 0.001, 0.001);
  //         scene.add(object2);

  //         var object3 = object.clone();
  //         object3.position.x = 68;
  //         object3.position.y = 0;
  //         object3.position.z = 138;

  //         object3.rotation.z = 0; //Rotattion
  //         object3.scale.set(0.001, 0.001, 0.001);
  //         scene.add(object3);

  //         var object4 = object.clone();
  //         object4.position.x = 68;
  //         object4.position.y = 0;
  //         object4.position.z = 228;

  //         object4.rotation.z = 0; //Rotattion
  //         object4.scale.set(0.001, 0.001, 0.001);
  //         scene.add(object4);

  //         var object5 = object.clone();
  //         object5.position.x = -65;
  //         object5.position.y = 0;
  //         object5.position.z = 138;

  //         object5.rotation.z = 0; //Rotattion
  //         object5.scale.set(0.001, 0.001, 0.001);
  //         scene.add(object5);

  //         var object6 = object.clone();
  //         object6.position.x = -65;
  //         object6.position.y = 0;
  //         object6.position.z = 228;

  //         object6.rotation.z = 0; //Rotattion
  //         object6.scale.set(0.001, 0.001, 0.001);
  //         scene.add(object6);
  //   });
  // }

  function loadsquare_door() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/square/square_door.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      var object1 = object.clone();
          object1.position.x = 68;
          object1.position.y = 0;
          object1.position.z = -50;

          object1.rotation.z = 0; //Rotattion
          object1.scale.set(0.001, 0.001, 0.001);
          scene.add(object1);

          var object2 = object.clone();
          object2.position.x = 68;
          object2.position.y = 0;
          object2.position.z = 35;

          object2.rotation.z = 0; //Rotattion
          object2.scale.set(0.001, 0.001, 0.001);
          scene.add(object2);

          var object3 = object.clone();
          object3.position.x = 68;
          object3.position.y = 0;
          object3.position.z = 138;

          object3.rotation.z = 0; //Rotattion
          object3.scale.set(0.001, 0.001, 0.001);
          scene.add(object3);

          var object4 = object.clone();
          object4.position.x = 68;
          object4.position.y = 0;
          object4.position.z = 228;

          object4.rotation.z = 0; //Rotattion
          object4.scale.set(0.001, 0.001, 0.001);
          scene.add(object4);

          var object5 = object.clone();
          object5.position.x = -65;
          object5.position.y = 0;
          object5.position.z = 138;

          object5.rotation.z = 0; //Rotattion
          object5.scale.set(0.001, 0.001, 0.001);
          scene.add(object5);

          var object6 = object.clone();
          object6.position.x = -65;
          object6.position.y = 0;
          object6.position.z = 228;

          object6.rotation.z = 0; //Rotattion
          object6.scale.set(0.001, 0.001, 0.001);
          scene.add(object6);
    });
  }

  function loadSquare_edges_park() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/square/square_edges_park.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      var object1 = object.clone();
          object1.position.x = 68;
          object1.position.y = 0;
          object1.position.z = -50;

          object1.rotation.z = 0; //Rotattion
          object1.scale.set(0.001, 0.001, 0.001);
          scene.add(object1);

          var object2 = object.clone();
          object2.position.x = 68;
          object2.position.y = 0;
          object2.position.z = 35;

          object2.rotation.z = 0; //Rotattion
          object2.scale.set(0.001, 0.001, 0.001);
          scene.add(object2);

          var object3 = object.clone();
          object3.position.x = 68;
          object3.position.y = 0;
          object3.position.z = 138;

          object3.rotation.z = 0; //Rotattion
          object3.scale.set(0.001, 0.001, 0.001);
          scene.add(object3);

          var object4 = object.clone();
          object4.position.x = 68;
          object4.position.y = 0;
          object4.position.z = 228;

          object4.rotation.z = 0; //Rotattion
          object4.scale.set(0.001, 0.001, 0.001);
          scene.add(object4);

          var object5 = object.clone();
          object5.position.x = -65;
          object5.position.y = 0;
          object5.position.z = 138;

          object5.rotation.z = 0; //Rotattion
          object5.scale.set(0.001, 0.001, 0.001);
          scene.add(object5);

          var object6 = object.clone();
          object6.position.x = -65;
          object6.position.y = 0;
          object6.position.z = 228;

          object6.rotation.z = 0; //Rotattion
          object6.scale.set(0.001, 0.001, 0.001);
          scene.add(object6);
    });
  }

  function loadSquare_top_park() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/square/square_top_park.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x666666 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      var object1 = object.clone();
          object1.position.x = 68;
          object1.position.y = 0;
          object1.position.z = -50;

          object1.rotation.z = 0; //Rotattion
          object1.scale.set(0.001, 0.001, 0.001);
          scene.add(object1);

          var object2 = object.clone();
          object2.position.x = 68;
          object2.position.y = 0;
          object2.position.z = 35;

          object2.rotation.z = 0; //Rotattion
          object2.scale.set(0.001, 0.001, 0.001);
          scene.add(object2);

          var object3 = object.clone();
          object3.position.x = 68;
          object3.position.y = 0;
          object3.position.z = 138;

          object3.rotation.z = 0; //Rotattion
          object3.scale.set(0.001, 0.001, 0.001);
          scene.add(object3);

          var object4 = object.clone();
          object4.position.x = 68;
          object4.position.y = 0;
          object4.position.z = 228;

          object4.rotation.z = 0; //Rotattion
          object4.scale.set(0.001, 0.001, 0.001);
          scene.add(object4);

          var object5 = object.clone();
          object5.position.x = -65;
          object5.position.y = 0;
          object5.position.z = 138;

          object5.rotation.z = 0; //Rotattion
          object5.scale.set(0.001, 0.001, 0.001);
          scene.add(object5);

          var object6 = object.clone();
          object6.position.x = -65;
          object6.position.y = 0;
          object6.position.z = 228;

          object6.rotation.z = 0; //Rotattion
          object6.scale.set(0.001, 0.001, 0.001);
          scene.add(object6);
    });
  }


  function loadSquare_wall_type1() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/square/square_wall_type1.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xC7C7C7 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      var object1 = object.clone();
          object1.position.x = 68;
          object1.position.y = 0;
          object1.position.z = -50;

          object1.rotation.z = 0; //Rotattion
          object1.scale.set(0.001, 0.001, 0.001);
          scene.add(object1);

          var object2 = object.clone();
          object2.position.x = 68;
          object2.position.y = 0;
          object2.position.z = 35;

          object2.rotation.z = 0; //Rotattion
          object2.scale.set(0.001, 0.001, 0.001);
          scene.add(object2);

          var object3 = object.clone();
          object3.position.x = 68;
          object3.position.y = 0;
          object3.position.z = 138;

          object3.rotation.z = 0; //Rotattion
          object3.scale.set(0.001, 0.001, 0.001);
          scene.add(object3);

          var object4 = object.clone();
          object4.position.x = 68;
          object4.position.y = 0;
          object4.position.z = 228;

          object4.rotation.z = 0; //Rotattion
          object4.scale.set(0.001, 0.001, 0.001);
          scene.add(object4);

          var object5 = object.clone();
          object5.position.x = -65;
          object5.position.y = 0;
          object5.position.z = 138;

          object5.rotation.z = 0; //Rotattion
          object5.scale.set(0.001, 0.001, 0.001);
          scene.add(object5);

          var object6 = object.clone();
          object6.position.x = -65;
          object6.position.y = 0;
          object6.position.z = 228;

          object6.rotation.z = 0; //Rotattion
          object6.scale.set(0.001, 0.001, 0.001);
          scene.add(object6);
    });
  }

  
  function loadSquare_wall_typeGold_park() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/square/square_wall_typeGold_park.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x666666 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      var object1 = object.clone();
          object1.position.x = 68;
          object1.position.y = 0;
          object1.position.z = -50;

          object1.rotation.z = 0; //Rotattion
          object1.scale.set(0.001, 0.001, 0.001);
          scene.add(object1);

          var object2 = object.clone();
          object2.position.x = 68;
          object2.position.y = 0;
          object2.position.z = 35;

          object2.rotation.z = 0; //Rotattion
          object2.scale.set(0.001, 0.001, 0.001);
          scene.add(object2);

          var object3 = object.clone();
          object3.position.x = 68;
          object3.position.y = 0;
          object3.position.z = 138;

          object3.rotation.z = 0; //Rotattion
          object3.scale.set(0.001, 0.001, 0.001);
          scene.add(object3);

          var object4 = object.clone();
          object4.position.x = 68;
          object4.position.y = 0;
          object4.position.z = 228;

          object4.rotation.z = 0; //Rotattion
          object4.scale.set(0.001, 0.001, 0.001);
          scene.add(object4);

          var object5 = object.clone();
          object5.position.x = -65;
          object5.position.y = 0;
          object5.position.z = 138;

          object5.rotation.z = 0; //Rotattion
          object5.scale.set(0.001, 0.001, 0.001);
          scene.add(object5);

          var object6 = object.clone();
          object6.position.x = -65;
          object6.position.y = 0;
          object6.position.z = 228;

          object6.rotation.z = 0; //Rotattion
          object6.scale.set(0.001, 0.001, 0.001);
          scene.add(object6);
    });
  }



    ///////////////////////LONG BUILDING


    function loadLong_shell()  {

    // prepare loader and load the model

        var loader = new THREE.FBXLoader( );
        loader.load( '../src/models/parkbasicmodels/long/long_shell.FBX', function( object, materials ) {
          // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

          object.traverse( function(child) {
            if (child instanceof THREE.Mesh) {
              // enable casting shadows
              // child.material = material2;
              child.castShadow = true;
              child.receiveShadow = true;
              // child.material.side = THREE.DoubleSide;
            }
          });
          
          object.position.x = -77;
          object.position.y = 0;
          object.position.z = -45;

          object.rotation.y = -Math.PI/2;
          object.rotation.z = 0; //Rotattion
          object.scale.set(0.001, 0.001, 0.001);
          scene.add(object);
        })
  }

    function loadLong_door() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/long/long_door.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -77;
      object.position.y = 0;
      object.position.z = -45;
      
      object.rotation.y = -Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }



    function loadLong_edges_park() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/long/long_edges_park.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -77;
      object.position.y = 0;
      object.position.z = -45;
      
      object.rotation.y = -Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }


  function loadLong_frame_park() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/long/long_frame_park.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -77;
      object.position.y = 0;
      object.position.z = -45;

      object.rotation.y = -Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }


  function loadLong_glasses_park() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/long/long_glasses_park.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var background = new THREE.CubeTextureLoader()
          .setPath( '../src/textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505, shininess: 10});
      var material2 = new THREE.MeshBasicMaterial( { color: 0xb9a988, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -77;
      object.position.y = 0;
      object.position.z = -45;
      
      object.rotation.y = -Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }

  function loadLong_top_park() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/long/long_top_park.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x666666 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -77;
      object.position.y = 0;
      object.position.z = -45;
      
      object.rotation.y = -Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }

  function loadLong_wall_gold_park() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/parkbasicmodels/long/long_wall_gold_park.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xC7C7C7 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -77;
      object.position.y = 0;
      object.position.z = -45;

      object.rotation.y = -Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }


















function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {

      var textures = [];

      for ( var i = 0; i < tilesNum; i ++ ) {

        textures[ i ] = new THREE.Texture();

      }

      var imageObj = new Image();

      imageObj.onload = function() {

        var canvas, context;
        var tileWidth = imageObj.height;

        for ( var i = 0; i < textures.length; i ++ ) {

          canvas = document.createElement( 'canvas' );
          context = canvas.getContext( '2d' );
          canvas.height = tileWidth;
          canvas.width = tileWidth;
          context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
          textures[ i ].image = canvas
          textures[ i ].needsUpdate = true;

        }

      };

      imageObj.src = atlasImgUrl;

      return textures;

    }

// Animate the scene
function animate() {
  requestAnimationFrame(animate);
  render();
  update();
}

// Update controls and stats
function update() {
  controls.update(clock.getDelta());
}


// Render the scene
function render() {
    renderer.render(scene, camera);
}

// Initialize lesson on page load
function initializeLesson() {
  init();
  animate();
}

if (window.addEventListener)
  window.addEventListener('load', initializeLesson, false);
else if (window.attachEvent)
  window.attachEvent('onload', initializeLesson);
else window.onload = initializeLesson;


function onDocumentMouseDown(event) {
        event.preventDefault();
        console.log("get")
        var vector = new THREE.Vector3((event.clientX / window.innerWidth) * 2 -
            1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
        vector.unproject(camera);


        var raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position)
            .normalize());
        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {
           window.open(intersects[0].object.userData.URL, "_self");
        }
      }
