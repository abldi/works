// var group;
// var cameraAnimation = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, .1, 2000 );
// var pipeSpline = [
//         new THREE.Vector3( 310, 80, 210 ),
//         new THREE.Vector3( 270, 80, 130 ),
//         new THREE.Vector3( 250, 10, -14 ),
//         new THREE.Vector3( 180, 2, -30 ),
//         new THREE.Vector3( 100, 2, -30 ),
//         new THREE.Vector3( 30, 2, -30 ),
//         new THREE.Vector3( -40, 2, -30 ),
//         new THREE.Vector3( -70, 2, -30 ),
//         new THREE.Vector3( -90, 2, -30 ),
//         new THREE.Vector3( -160, 2, -30 ),
//         new THREE.Vector3( -210, 2, -30 ),//11
//         new THREE.Vector3( -280, 10, -45 ),
//         new THREE.Vector3( -300, 40, -100 ),
//         new THREE.Vector3( -210, 60, -190 ),
//         new THREE.Vector3( -50, 80, -100 ),
//         new THREE.Vector3( -30, 80, -20 ),
//         new THREE.Vector3( -25, 60, 50 ),
//         new THREE.Vector3( -5, 60, 120 ),
//         new THREE.Vector3( 90, 60, 190 ),
//         new THREE.Vector3( 150, 70, 180 ),//20
//         new THREE.Vector3( 250, 80, 210 )
//     ]

// var pipeSpline2 = [
//         new THREE.Vector3( 130, 0, 0 ),
//         new THREE.Vector3( 100, 0, -10 ),
//         new THREE.Vector3( 80, 0, -26 ),
//         new THREE.Vector3( 40, 0, -30 ),
//         new THREE.Vector3( -10, 2, -30 ),
//         new THREE.Vector3( -20, 2, 0 ),
//         new THREE.Vector3( -25, 2, 0 ),
//         new THREE.Vector3( -120, 2, -10 ),
//         new THREE.Vector3( -200, 2, -30 ),
//         new THREE.Vector3( -240, 2, -30 ),
//         new THREE.Vector3( -280, 2, -90 ),//11
//         new THREE.Vector3( -260, 2, -120 ),
//         new THREE.Vector3( -190, 2, -140 ),
//         new THREE.Vector3( -120, 2, -100 ),
//         new THREE.Vector3( 20, 2, -10 ),
//         new THREE.Vector3( 30, 2, 50 ),
//         new THREE.Vector3( 60, 2, 90 ),
//         new THREE.Vector3( 70, 2, 100 ),
//         new THREE.Vector3( 100, 2, 110 ),
//         new THREE.Vector3( 120, 2, 105 ),
//         new THREE.Vector3( 140, 2, 50 )
//     ]

// var spline = new THREE.SplineCurve3(pipeSpline);
// var spline2 = new THREE.SplineCurve3(pipeSpline2);

var camPosIndex = 0;

var scene;
var camera;
var renderer = null;
var container = null;
var controls = null;
var clock = null;
var stats = null;




  function init() { // Initialization

    THREE.DefaultLoadingManager.onProgress = function ( item, loaded, total ) {

          $('.loadingbarcolor')[0].style.width = Math.min(100, parseInt(100.0 * loaded / total)) + '%';

          console.log(Math.min(100, parseInt(100.0 * loaded / total)));

          if (Math.min(100, parseInt(100.0 * loaded / total)) == 100) {
              $('.showload')[0].style.display = 'none';
          }

          return Math.min(100, parseInt(100.0 * loaded / total));
          //console.log( item, loaded, total );
      };

    // create main scene
    scene = new THREE.Scene();
    scene.fog = new THREE.FogExp2(0xcce0ff, 0.0018);


    







    group = new THREE.Group();
       scene.add( group );

    var SCREEN_WIDTH = window.innerWidth,

        SCREEN_HEIGHT = window.innerHeight;
        console.log(SCREEN_WIDTH);
    // prepare camera
    var VIEW_ANGLE = 60, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 1, FAR = 2000;
    camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
    scene.add(camera);
    camera.position.set(200, 100, 300);
    // camera.lookAt(new THREE.Vector3(0,0,0));



    // prepare renderer
    renderer = new THREE.WebGLRenderer({ antialias:true });

    renderer.setPixelRatio(window.devicePixelRatio);

    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    renderer.setClearColor(scene.fog.color);
    renderer.shadowMapEnabled = true;
    renderer.shadowMapSoft = true;

    // prepare container
    container = document.createElement('div');
    document.body.appendChild(container);
    container.appendChild(renderer.domElement);

    // events
    THREEx.WindowResize(renderer, camera);

    // // prepare controls (OrbitControls)
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.target = new THREE.Vector3(0, 0, 0)
    controls.minDistance = 50;
    controls.maxDistance = 400;
    controls.maxPolarAngle = Math.PI/2.05; 

    // controls.center.set(spline2.getPoint(camPosIndex / 500));
    camera.position.copy(controls.center).add(new THREE.Vector3(200, 200, 200));

    // prepare clock
    clock = new THREE.Clock();


    scene.add(new THREE.AmbientLight(0x576095));

    var light;

    light = new THREE.DirectionalLight(0xffffff, 1.0);//e0f3f7
    light.position.set(-200, 400, 300);
    light.position.multiplyScalar(1);

    light.castShadow = true;
    light.shadowCameraVisible = true;
    light.shadow.bias = 0.0001; // use bias to tweak

    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;

    var d = 240;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;

    scene.add(light);



    var lightL;

    lightL = new THREE.DirectionalLight(0xdfebff, 0.4);
    lightL.position.set(-200, 400, -300);
    lightL.position.multiplyScalar(1);

    lightL.castShadow = false;
    lightL.shadowCameraVisible = false;
    light.shadow.bias = -0.0005; // use bias to tweak


    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;
    var d = 240;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;
    scene.add(lightL);

    light = new THREE.HemisphereLight(0xffffff, 0x444444, 1.0);
        light.position.set(0, 1, 0);
        scene.add(light);



    // add simple ground
    // var ground = new THREE.Mesh( new THREE.PlaneGeometry(20, 20, 10, 10), new THREE.MeshPhongMaterial({color: 0xff0000}) );
    // ground.position.set(0, 10, 0);
    // ground.rotation.x = -Math.PI / 2;
    // scene.add(ground);


      var loader = new THREE.FBXLoader( );
        loader.load( 'models/airplane/Ground.FBX', function( object, materials ) {
          // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

          object.traverse( function(child) {
            if (child instanceof THREE.Mesh) {
              // enable casting shadows
              // child.material = material2;
              child.castShadow = true;
              child.receiveShadow = true;
              child.material.side = THREE.DoubleSide;
            }
          });
          
          object.position.x = 0;
          object.position.y = -0.1;
          object.position.z = 0;

          object.rotation.z = 0; //Rotattion
          object.scale.set(0.06, 0.06, 0.06);
          scene.add(object);


        })



      var loadersky = new THREE.TextureLoader();
        loadersky.load( 'textures/skyboxflip.jpg', function ( texture ) {

          var geometry = new THREE.SphereGeometry( -1000, 10, 10 );


          var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 1 } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.y = Math.PI;
          group.add( mesh );

        } );


    // loadModelbuilding();
    // loadModelwall();
    // loadModelwindow();
    // loadModelH();
    // loadModelbase();
    // // loadModelceiling();
    // loadModelceilingglass();
    // // loadModelfloor();
    // loadModelglass();
    // // loadModelstreet();
    // loadModelgreen();
    // loadModelwingwindow();
    // // loadModelwingbody();
    // // loadModelwingceiling();
    // // loadModeloutsidefloor();
    // loadModelwingframe();
    // loadModelsidebuilding();
    // // loadModelcarslot();
    // loadModelall();
    // loadModelfloortexture();
    // loadModeledge();


    loadLong_shell();

  }

  function loadLong_shell()  {

    // prepare loader and load the model

        var loader = new THREE.FBXLoader( );
        loader.load( 'models/long/long_shell.FBX', function( object, materials ) {
          // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

          object.traverse( function(child) {
            if (child instanceof THREE.Mesh) {
              // enable casting shadows
              // child.material = material2;
              child.castShadow = true;
              child.receiveShadow = true;
              child.material.side = THREE.DoubleSide;
            }
          });
          
          object.position.x = 0;
          object.position.y = 0;
          object.position.z = 0;

          object.rotation.z = 0; //Rotattion
          object.scale.set(0.001, 0.001, 0.001);
          scene.add(object);


        })

      

  }

  function loadModeledge() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/edge.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x666666 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelfloortexture()  {

    // prepare loader and load the model

        var loader = new THREE.FBXLoader( );
        loader.load( 'models/airplane/FloorF.FBX', function( object, materials ) {
          // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

          object.traverse( function(child) {
            if (child instanceof THREE.Mesh) {
              // enable casting shadows
              // child.material = material2;
              child.castShadow = true;
              child.receiveShadow = true;
              child.material.side = THREE.DoubleSide;
            }
          });
          
          object.position.x = 0;
          object.position.y = 0;
          object.position.z = 0;

          object.rotation.z = 0; //Rotattion
          object.scale.set(0.06, 0.06, 0.06);
          scene.add(object);


        })

      

  }
  function loadModelall()  {

    // prepare loader and load the model

        var loader = new THREE.FBXLoader( );
        loader.load( 'models/airplane/all.FBX', function( object, materials ) {
          // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

          object.traverse( function(child) {
            if (child instanceof THREE.Mesh) {
              // enable casting shadows
              // child.material = material2;
              child.castShadow = true;
              child.receiveShadow = true;
            }
          });
          
          object.position.x = 0;
          object.position.y = 0;
          object.position.z = 0;

          object.rotation.z = 0; //Rotattion
          object.scale.set(0.06, 0.06, 0.06);
          scene.add(object);


        })

      

  }
  function loadModelcarslot()  {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/carslot.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x253608 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0.1;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelsidebuilding() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/sidebuilding.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({color: 0x2d4dc5, transparent: true, opacity: 0.4});

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = false;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelwingwindow() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/wingwindow.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505, shininess: 10});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x185777, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = false;


        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);

    });
  }
  function loadModelwingframe() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/wingframe.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x333333 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelwingceiling() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/wingceiling.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelwingbody(){

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/wingbody.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModeloutsidefloor() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/outsidefloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x828282 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }

  function loadModelfloor() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/floor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x828282 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelbase() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/base.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xa5a5a5 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelceilingglass() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/ceilingglass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505, shininess: 10});
      var material2 = new THREE.MeshBasicMaterial( { color: 0xbfe2fd, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = false;


        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);

    });
  }
  function loadModelceiling() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/ceiling.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelgreen() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/green.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x081904});

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }

  function loadModelH() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/H.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x343434 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }

  function loadModelwall() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }

   function loadModelwindow() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/window.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x333333 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }


  function loadModelglass() {




    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505, shininess: 10});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x185777, envMap: background } );


      object.traverse( function( node ) {
          if( node.material ) {
              node.material.side = THREE.DoubleSide;
          }
      });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = false;


        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);

    });
  }

  function loadModelstreet() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/street.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x818181 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }



function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {

      var textures = [];

      for ( var i = 0; i < tilesNum; i ++ ) {

        textures[ i ] = new THREE.Texture();

      }

      var imageObj = new Image();

      imageObj.onload = function() {

        var canvas, context;
        var tileWidth = imageObj.height;

        for ( var i = 0; i < textures.length; i ++ ) {

          canvas = document.createElement( 'canvas' );
          context = canvas.getContext( '2d' );
          canvas.height = tileWidth;
          canvas.width = tileWidth;
          context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
          textures[ i ].image = canvas
          textures[ i ].needsUpdate = true;

        }

      };

      imageObj.src = atlasImgUrl;

      return textures;

    }

// Animate the scene
function animate() {
  requestAnimationFrame(animate);
  render();
  update();
}

// Update controls and stats
function update() {


  
  


  controls.update(clock.getDelta());
}


// Render the scene
function render() {

    renderer.render(scene, camera);
  
}

// Initialize lesson on page load
function initializeLesson() {
  init();
  animate();
}

if (window.addEventListener)
  window.addEventListener('load', initializeLesson, false);
else if (window.attachEvent)
  window.attachEvent('onload', initializeLesson);
else window.onload = initializeLesson;


function onDocumentMouseDown(event) {
        event.preventDefault();
        console.log("get")
        var vector = new THREE.Vector3((event.clientX / window.innerWidth) * 2 -
            1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
        vector.unproject(camera);


        var raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position)
            .normalize());
        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {
           window.open(intersects[0].object.userData.URL, "_self");
        }
      }
