
var camPosIndex = 0;

var scene;
var camera;
var renderer = null;
var container = null;
var controls = null;
var clock = null;
var stats = null;




  function init() { // Initialization

    THREE.DefaultLoadingManager.onProgress = function ( item, loaded, total ) {

          $('.loadingbarcolor')[0].style.width = Math.min(100, parseInt(100.0 * loaded / total)) + '%';

          console.log(Math.min(100, parseInt(100.0 * loaded / total)));

          if (Math.min(100, parseInt(100.0 * loaded / total)) == 100) {
              $('.showload')[0].style.display = 'none';
          }

          return Math.min(100, parseInt(100.0 * loaded / total));
          //console.log( item, loaded, total );
      };

    // create main scene
    scene = new THREE.Scene();
    scene.fog = new THREE.FogExp2(0xcce0ff, 0.0018);


    







    group = new THREE.Group();
       scene.add( group );

    var SCREEN_WIDTH = window.innerWidth,

        SCREEN_HEIGHT = window.innerHeight;
        console.log(SCREEN_WIDTH);
    // prepare camera
    var VIEW_ANGLE = 60, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 1, FAR = 2000;
    camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
    console.log('camera position');
    console.log(camera.position);
    // camera.position.set(0, 0, 300);
    // this.camera.position.set(140, 5, 200);
    
    // camera.lookAt(new THREE.Vector3(0,0,0));

    scene.add(camera);

    // prepare renderer
    renderer = new THREE.WebGLRenderer({ antialias:true });

    renderer.setPixelRatio(window.devicePixelRatio);

    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    renderer.setClearColor(scene.fog.color);
    renderer.shadowMapEnabled = true;
    renderer.shadowMapSoft = true;

    // prepare container
    container = document.createElement('div');
    document.body.appendChild(container);
    container.appendChild(renderer.domElement);

    // events
    THREEx.WindowResize(renderer, camera);

    // // prepare controls (OrbitControls)
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.target = new THREE.Vector3(0, 0, 0)
    controls.minDistance = 70;
    controls.maxDistance = 140;
    controls.maxPolarAngle = Math.PI/2.05; 

    // controls.center.set(spline2.getPoint(camPosIndex / 500));
    camera.position.copy(controls.center).add(new THREE.Vector3(60, 5, 60));

    // prepare clock
    clock = new THREE.Clock();


    scene.add(new THREE.AmbientLight(0x576095));

    var light;

    light = new THREE.DirectionalLight(0xffffff, .1);//e0f3f7
    light.position.set(-200, 400, 300);
    light.position.multiplyScalar(1);

    light.castShadow = true;
    light.shadowCameraVisible = true;
    light.shadow.bias = 0.0001; // use bias to tweak

    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;

    var d = 240;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;

    scene.add(light);



    var lightL;

    lightL = new THREE.DirectionalLight(0xdfebff, 0.9);
    lightL.position.set(-200, 400, -300);
    lightL.position.multiplyScalar(1);

    lightL.castShadow = false;
    lightL.shadowCameraVisible = false;
    light.shadow.bias = -0.0005; // use bias to tweak


    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;
    var d = 240;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;
    scene.add(lightL);

    light = new THREE.HemisphereLight(0xffffff, 0x444444, 1.0);
        light.position.set(0, 1, 0);
        scene.add(light);



    // add simple ground
    // var ground = new THREE.Mesh( new THREE.PlaneGeometry(20, 20, 10, 10), new THREE.MeshPhongMaterial({color: 0xff0000}) );
    // ground.position.set(0, 10, 0);
    // ground.rotation.x = -Math.PI / 2;
    // scene.add(ground);
    // var ground = new THREE.Mesh( new THREE.PlaneGeometry(1000, 1000, 10, 10), new THREE.MeshLambertMaterial('../src/textures/land2.jpg') );
    // ground.receiveShadow = true;
    // ground.position.set(0, 0, 0);
    // ground.rotation.x = -Math.PI / 2;
    // scene.add(ground);


      var loader = new THREE.FBXLoader( );
        loader.load( '../src/models/ground.FBX', function( object, materials ) {
          // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

          object.traverse( function(child) {
            if (child instanceof THREE.Mesh) {
              // enable casting shadows
              // child.material = material2;
              child.castShadow = true;
              child.receiveShadow = true;
              child.material.side = THREE.DoubleSide;
            }
          });
          
          object.position.x = 0;
          object.position.y = 1;
          object.position.z = 35;

          object.rotation.z = 0; //Rotattion
          object.scale.set(0.7, 0.7, 0.7);
          scene.add(object);


        })



      var loadersky = new THREE.TextureLoader();
        loadersky.load( '../src/textures/skyboxflip.jpg', function ( texture ) {

          var geometry = new THREE.SphereGeometry( -1000, 10, 10 );


          var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 1 } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.y = Math.PI;
          group.add( mesh );

        } );



    // loadModelbuilding();
    // loadModelwall();
    // loadModelwindow();
    // loadModelH();

    // loadModelbase();
    // loadModelceiling();
    // loadModelceilingglass();
    // loadModelfloor();
    // loadModelglass();
    // loadModelstreet();
    // loadModelgreen();
    // loadModelwingwindow();
    // loadModelwingbody();
    // loadModelwingceiling();
    // loadModeloutsidefloor();
    // loadModelwingframe();
    // loadModelsidebuilding();
    // loadModelcarslot();
    // loadModelall();
    // loadModeledge();

    // loadSquare_shell();
    // loadSquare_glasses_park();
    // loadSquare_shell_park();
    // loadSquare_edges_park();
    // loadSquare_top_park();
    // loadSquare_wall_type1_park();
    // loadSquare_wall_typeGold_park();


    loadsquare_door();
    loadsquare_edges();
    loadsquare_foundation();
    loadsquare_glasses();
    loadsquare_shell();
    loadsquare_top();
    loadsquare_wall_type_gold();
    loadsquare_wall_type1();
    loadsquare_backwall();
    // loadBGround();

  }


  // function loadBGround() {

  //   // prepare loader and load the model
  //   var oLoader = new THREE.OBJLoader();
  //   oLoader.load('../src/models/FBbreakdown/BGroundFloor.obj', function(object, materials) {

  //     // var material = new THREE.MeshFaceMaterial(materials);
  //     var material2 = new THREE.MeshPhongMaterial({ color: 0x555555, shading: THREE.SmoothShading });

  //     object.traverse( function(child) {
  //       if (child instanceof THREE.Mesh) {

  //         // apply custom material
  //         child.material = material2;

  //         // enable casting shadows
  //         child.castShadow = true;
  //         child.receiveShadow = true;
  //       }
  //     });
      
  //     //original
  //     object.position.x = -150;
  //     object.position.y = 0;
  //     object.position.z = 90;
  //     object.rotation.x = - Math.PI/2;
  //     object.rotation.z = 0; //rotattion
  //     object.scale.set(0.8, 0.8, 0.8);
  //     scene.add(object);

  //   });
  // }



  function loadsquare_shell()  {

    // prepare loader and load the model

        var loader = new THREE.FBXLoader( );
        loader.load( '../src/models/square/square_shell.FBX', function( object, materials ) {
          // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

          object.traverse( function(child) {
            if (child instanceof THREE.Mesh) {
              // enable casting shadows
              // child.material = material2;
              child.castShadow = true;
              child.receiveShadow = true;
              child.material.side = THREE.DoubleSide;
            }
          });
          
          object.position.x = 0;
          object.position.y = -1.3;
          object.position.z = 40;

          object.rotation.z = 0; //Rotattion
          object.scale.set(0.001, 0.001, 0.001);
          scene.add(object);
        })
  }

  function loadsquare_backwall()  {

    // prepare loader and load the model

        var loader = new THREE.FBXLoader( );
        loader.load( '../src/models/square/square_backwall.FBX', function( object, materials ) {
          // var material2 = new THREE.MeshPhongMaterial({specular: 0xf19900});

          object.traverse( function(child) {
            if (child instanceof THREE.Mesh) {
              // enable casting shadows
              // child.material = material2;
              child.castShadow = true;
              child.receiveShadow = true;
              child.material.side = THREE.DoubleSide;
            }
          });
          
          object.position.x = 0;
          object.position.y = -1.3;
          object.position.z = 40;

          object.rotation.z = 0; //Rotattion
          object.scale.set(0.001, 0.001, 0.001);
          scene.add(object);
        })
  }


  function loadsquare_door() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/square/square_door.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xC7C7C7 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = -1.3;
      object.position.z = 40;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }


  function loadsquare_edges() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/square/square_edges.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = -1.3;
      object.position.z = 40;
      object.rotation.x = 0;

      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }


  function loadsquare_foundation() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/square/square_foundation.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x444444, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = -1.2;
      object.position.z = 40;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }



  function loadsquare_glasses() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/square/square_glasses.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var background = new THREE.CubeTextureLoader()
          .setPath( '../src/textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505, shininess: 10});
      var material2 = new THREE.MeshBasicMaterial( { color: 0xb9a988, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = -1.3;
      object.position.z = 40;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }


  function loadsquare_top() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/square/square_top.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x666666 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = -1.3;
      object.position.z = 40;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }

  
  function loadsquare_wall_type_gold() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/square/square_wall_type_gold.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x666666 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = -1.3;
      object.position.z = 40;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }


  function loadsquare_wall_type1() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('../src/models/square/square_wall_type1.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xC7C7C7 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          // child.material.side = THREE.DoubleSide;
          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = -1.3;
      object.position.z = 40;
      object.rotation.x = 0;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.001, 0.001, 0.001);
      scene.add(object);
    });
  }





function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {

      var textures = [];

      for ( var i = 0; i < tilesNum; i ++ ) {

        textures[ i ] = new THREE.Texture();

      }

      var imageObj = new Image();

      imageObj.onload = function() {

        var canvas, context;
        var tileWidth = imageObj.height;

        for ( var i = 0; i < textures.length; i ++ ) {

          canvas = document.createElement( 'canvas' );
          context = canvas.getContext( '2d' );
          canvas.height = tileWidth;
          canvas.width = tileWidth;
          context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
          textures[ i ].image = canvas
          textures[ i ].needsUpdate = true;

        }

      };

      imageObj.src = atlasImgUrl;

      return textures;

    }

// Animate the scene
function animate() {
  requestAnimationFrame(animate);
  render();
  update();
}

// Update controls and stats
function update() {


  
  


  controls.update(clock.getDelta());
}


// Render the scene
function render() {

    renderer.render(scene, camera);
  
}

// Initialize lesson on page load
function initializeLesson() {
  init();
  animate();
}

if (window.addEventListener)
  window.addEventListener('load', initializeLesson, false);
else if (window.attachEvent)
  window.attachEvent('onload', initializeLesson);
else window.onload = initializeLesson;


function onDocumentMouseDown(event) {
        event.preventDefault();
        console.log("get")
        var vector = new THREE.Vector3((event.clientX / window.innerWidth) * 2 -
            1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
        vector.unproject(camera);


        var raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position)
            .normalize());
        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {
           window.open(intersects[0].object.userData.URL, "_self");
        }
      }
