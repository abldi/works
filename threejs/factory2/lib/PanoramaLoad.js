/**
 * Created by zhou-fu on 17-10-16.
 */

function DefaultPanoramaLoad() {

    this.load = function ( data, scene, geometry ) {

        if (!data){
            console.warn("Panorama data is null");
            return null;
        }

        console.log(data.type)
        switch (data.type){
            case 'equirectangular':{
                let material_ = new THREE.MeshBasicMaterial({color: 0xffffff});
                let material_s, material_l;

                let mesh = new THREE.Mesh( geometry, material_ );
                scene.add( mesh );

                material_s = new THREE.MeshBasicMaterial( {
                    map: new THREE.TextureLoader().load( data.panorama.small, function(texture){
                        if (material_s && mesh.material.uuid === material_.uuid){
                            mesh.material = material_s;
                        }
                    })
                } );

                material_l = new THREE.MeshBasicMaterial( {
                    map: new THREE.TextureLoader().load( data.panorama.large, function(texture){
                        if (material_l){
                            mesh.material = material_l;
                        }
                    })
                } );

                console.log(material_l)

                break;
            }

            case 'cube':{

                // let viewer = new PANOLENS.Viewer();

                let textures = [];
                let images = [];
                let canvas = [];

                let textures_high = [];
                let images_high = [];
                let canvas_high = [];

                let obj_;

                let interval_low = 5;
                let interval_high = 45;

                let loader = new THREE.ObjectLoader();

                for (let k = 0; k < 96; k++){
                    textures[ k ] = new THREE.Texture();

                    images[k] = new Image();
                    images[k].crossOrigin = "anonymous";

                    canvas[k] = document.createElement( 'canvas' );
//            context.filter = 'blur(500px)';
                    canvas[k].height = 128;
                    canvas[k].width = 128;

                    textures_high[ k ] = new THREE.Texture();

                    images_high[k] = new Image();
                    images_high[k].crossOrigin = "anonymous";

                    canvas_high[k] = document.createElement( 'canvas' );
//            context.filter = 'blur(500px)';
                    canvas_high[k].height = 512;
                    canvas_high[k].width = 512;
                }

                loader.load(
                    // resource URL
                    "model/test_model.json",

                    // pass the loaded data to the onLoad function.
//Here it is assumed to be an object
                    function ( obj ) {
                        //add the loaded object to the scene
                        obj_ = obj;

                        let i = 0;

                        LoadImageLowByTime(i);

                        // scene.add( obj_ );
                        console.log(obj_)
                        obj_.add(geometry);
                        scene.add( obj_ );
                        // scene.add(geometry);

                        LoadImageHighByTime(i);
                    },

                    // Function called when download progresses
                    function ( xhr ) {
                        console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
                    },

                    // Function called when download errors
                    function ( xhr ) {
                        console.error( 'An error happened' );
                    }
                );

                function LoadImageLowByTime (i) {           //  create a loop function
                    setTimeout(function () {    //  call a 3s setTimeout when the loop is called
                        OnLoadImage(i, canvas, textures, images, data.panorama.low);        //  your code here
                        i++;                     //  increment the counter
                        if (i < 96) {            //  if the counter < 10, call the loop function
                            LoadImageLowByTime(i);             //  ..  again which will trigger another
                        }                        //  ..  setTimeout()
                    }, interval_low)
                }

                function LoadImageHighByTime (i) {           //  create a loop function
                    setTimeout(function () {    //  call a 3s setTimeout when the loop is called
                        OnLoadImage(i, canvas_high, textures_high, images_high, data.panorama.high);        //  your code here
                        i++;                     //  increment the counter
                        if (i < 96) {            //  if the counter < 10, call the loop function
                            LoadImageHighByTime(i);             //  ..  again which will trigger another
                        }                        //  ..  setTimeout()
                    }, interval_high)
                }


                function OnLoadImage(number, canvas, textures, images, data){
                    let context = canvas[number].getContext( '2d' );
                    images[number].onload = function(){
//                context.drawImage( image, x, y);
                        context.drawImage(images[number], 0, 0);
                        textures[number].image = canvas[number];
                        textures[number].needsUpdate = true;

                        obj_.children[number].material =  new THREE.MeshStandardMaterial( {
                            emissiveMap: textures[number],
                            emissive: new THREE.Color("rgb(255, 255, 255)")
                        });
                    }
                    images[number].src = data[number];
                }

            }

        }
    };
}

let PanoramaLoad = new DefaultPanoramaLoad();