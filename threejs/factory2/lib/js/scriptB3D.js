if ( ! Detector.webgl ) {
 Detector.addGetWebGLMessage()
 $('.showload')[0].style.display = 'none';
}

var group;
var lesson6 = {
  scene: null,
  camera: null,
  renderer: null,
  container: null,
  controls: null,
  clock: null,
  stats: null,


  init: function() { // Initialization

    THREE.DefaultLoadingManager.onProgress = function ( item, loaded, total ) {

          $('.loadingbarcolor')[0].style.width = Math.min(100, parseInt(100.0 * loaded / total)) + '%';

          console.log(Math.min(100, parseInt(100.0 * loaded / total)));

          if (Math.min(100, parseInt(100.0 * loaded / total)) == 100) {
              $('.showload')[0].style.display = 'none';
          }

          return Math.min(100, parseInt(100.0 * loaded / total));
          //console.log( item, loaded, total );
      };



    // create main scene
    this.scene = new THREE.Scene();
    this.scene.fog = new THREE.FogExp2(0xcce0ff, 0.002);



    group = new THREE.Group();
       this.scene.add( group );

    var SCREEN_WIDTH = window.innerWidth,

        SCREEN_HEIGHT = window.innerHeight;
        console.log(SCREEN_WIDTH);
    // prepare camera
    var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 1, FAR = 2000;
    this.camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
    this.scene.add(this.camera);
    this.camera.position.set(140, 5, 200);
    this.camera.lookAt(new THREE.Vector3(0,0,0));

    // prepare renderer
    this.renderer = new THREE.WebGLRenderer({ antialias:true });
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    this.renderer.setClearColor(this.scene.fog.color);
    this.renderer.shadowMapEnabled = true;
    this.renderer.shadowMapSoft = true;

    // prepare container
    this.container = document.createElement('div');
    document.body.appendChild(this.container);
    this.container.appendChild(this.renderer.domElement);

    // events
    THREEx.WindowResize(this.renderer, this.camera);

    // prepare controls (OrbitControls)
    this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
    this.controls.target = new THREE.Vector3(0, 0, 10);
    this.controls.minDistance = 150;
    this.controls.maxDistance = 300;
    this.controls.maxPolarAngle = Math.PI/2.05; 

    // prepare clock
    this.clock = new THREE.Clock();


    // prepare stats
    // this.stats = new Stats();
    // this.stats.domElement.style.position = 'absolute';
    // this.stats.domElement.style.left = '50px';
    // this.stats.domElement.style.bottom = '50px';
    // this.stats.domElement.style.zIndex = 1;
    // this.container.appendChild( this.stats.domElement );

    // add ambient light

    this.scene.add(new THREE.AmbientLight(0xb8bee0));

    var light;

    light = new THREE.DirectionalLight(0xf8eac7, 0.7);
    light.position.set(-200, 400, 300);
    light.position.multiplyScalar(1);

    light.castShadow = true;
    light.shadowCameraVisible = true;
    light.shadow.bias = -0.0002;


    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;

    var d = 200;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;

    this.scene.add(light);

    var lightL;

    lightL = new THREE.DirectionalLight(0xdfebff, 0.3);
    lightL.position.set(-200, 400, -300);
    lightL.position.multiplyScalar(1);

    lightL.castShadow = false;
    lightL.shadowCameraVisible = false;
    this.scene.add(lightL);




    // add simple ground
    var ground = new THREE.Mesh( new THREE.PlaneGeometry(1000, 1000, 10, 10), new THREE.MeshLambertMaterial('textures/land.jpg') );
    ground.receiveShadow = true;
    ground.position.set(0, 0, 0);
    ground.rotation.x = -Math.PI / 2;
    // this.scene.add(ground);


      // var loaderland = new THREE.TextureLoader();
      //   loaderland.load( 'textures/land1.png  ', function ( texture ) {

      //     var geometry = new THREE.PlaneGeometry(2000, 2000, 10, 10);



      //     // var material = new THREE.MeshLambertMaterial( { map: texture, overdraw: 1, shininess: 0 } );
      //     var material = new THREE.MeshLambertMaterial( { color: 0xbdae90 } );
      //     var mesh = new THREE.Mesh( geometry, material );
      //     mesh.rotation.x = - Math.PI/2;
      //     mesh.rotation.z = Math.PI/3;
      //     mesh.receiveShadow = true;
      //     mesh.position.set(0, -0.1, 0);

      //     this.group.add( mesh );

      //   } );


      var loadersky = new THREE.TextureLoader();
        loadersky.load( 'textures/skyboxflip.jpg', function ( texture ) {

          var geometry = new THREE.SphereGeometry( -1000, 10, 10 );


          var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 1 } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.y = Math.PI;
          this.group.add( mesh );

        } );

    // var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA.png" );
    // var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    // var sprite = new THREE.Sprite( materialsprite );
    // sprite.position.x = -36;
    // sprite.position.y = 27;
    // sprite.position.z = -19;
    // sprite.scale.x = 10;
    // sprite.scale.y = 10;
    // sprite.scale.z = 10;
    // this.scene.add( sprite );

    // var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteB.png" );
    // var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    // var sprite = new THREE.Sprite( materialsprite );
    // sprite.position.x = 0;
    // sprite.position.y = 25;
    // sprite.position.z = 0;
    // sprite.scale.x = 10;
    // sprite.scale.y = 10;
    // sprite.scale.z = 10;
    // this.scene.add( sprite );

    // var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteC.png" );
    // var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    // var sprite = new THREE.Sprite( materialsprite );
    // sprite.position.x = 85;
    // sprite.position.y = 25;
    // sprite.position.z = -65;
    // sprite.scale.x = 10;
    // sprite.scale.y = 10;
    // sprite.scale.z = 10;
    // this.scene.add( sprite );



    // load a model

    this.loadBFloor();
    // this.loadBFrameBlack();
    // this.loadBFrameSilver();
    this.loadBGlass();
    this.loadBGrass();
    this.loadBGround();
    this.loadBGroundFloor();
    this.loadBTiles();
    this.loadBWall();
    this.loadBEdge();
    this.loadBInner();
    this.loadBG();

    // this.loadModelwater();
    // this.loadModelroad();
  },

  loadBG: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BG.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xbdae90, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = -0.2;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },

  loadBFloor: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ specular: 0xe1e1e1, reflectivity: 0.52, shininess: 30 ,color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },


  loadBInner: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BInner.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x222222, transparent: true, opacity: 0.9 ,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },



  loadBFrameBlack: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BFrameBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x444444, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },





  loadBGrass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BGrass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },

  loadBGround: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BGround.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x777777, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },

  loadBGroundFloor: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BGroundFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x555555, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },


  loadBTiles: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BTiles.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ specular: 0xe1e1e1, reflectivity: 0.52, shininess: 30 ,color: 0xeeeeee, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },


  loadBWall: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BWallS.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ specular: 0xe1e1e1, reflectivity: 0.52, shininess: 30 ,color: 0xcccccc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },

  loadBEdge: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BEdge.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xcccccc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },




  

  //LoadBGlass

  loadBGlass: function() {




    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BGlass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505, shininess: 10});
      var material2 = new THREE.MeshBasicMaterial( { color: 0xb9a988, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -150;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);

    });
  },



  loadModelwater: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/water.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ specular: 0xe1e1e1, reflectivity: 0.52, shininess: 30 ,color: 0x7dbeff });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -85
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);
    });
  },

  loadModelroad: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/road.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ specular: 0xe1e1e1, reflectivity: 0.52, shininess: 30 ,color: 0xefb378 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -85
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.8, 0.8, 0.8);
      lesson6.scene.add(object);
    });
  }

};

function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {

      var textures = [];

      for ( var i = 0; i < tilesNum; i ++ ) {

        textures[ i ] = new THREE.Texture();

      }

      var imageObj = new Image();

      imageObj.onload = function() {

        var canvas, context;
        var tileWidth = imageObj.height;

        for ( var i = 0; i < textures.length; i ++ ) {

          canvas = document.createElement( 'canvas' );
          context = canvas.getContext( '2d' );
          canvas.height = tileWidth;
          canvas.width = tileWidth;
          context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
          textures[ i ].image = canvas
          textures[ i ].needsUpdate = true;

        }

      };

      imageObj.src = atlasImgUrl;

      return textures;

    }

// Animate the scene
function animate() {
  requestAnimationFrame(animate);
  render();
  update();
}

// Update controls and stats
function update() {
  lesson6.controls.update(lesson6.clock.getDelta());
}

// Render the scene
function render() {

  lesson6.renderer.render(lesson6.scene, lesson6.camera);
}

// Initialize lesson on page load
function initializeLesson() {
  lesson6.init();
  animate();
}

if (window.addEventListener)
  window.addEventListener('load', initializeLesson, false);
else if (window.attachEvent)
  window.attachEvent('onload', initializeLesson);
else window.onload = initializeLesson;
