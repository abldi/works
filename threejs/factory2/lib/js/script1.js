/**
 *
 * WebGL With Three.js - Lesson 6 - loading models
 * http://www.script-tutorials.com/webgl-with-three-js-lesson-6/
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Script Tutorials
 * http://www.script-tutorials.com/
 */
var group;
var lesson6 = {
  scene: null,
  camera: null,
  renderer: null,
  container: null,
  controls: null,
  clock: null,
  stats: null,


  init: function() { // Initialization



    // create main scene
    this.scene = new THREE.Scene();
    this.scene.fog = new THREE.FogExp2(0xcce0ff, 0.0015);



    group = new THREE.Group();
       this.scene.add( group );

    var SCREEN_WIDTH = window.innerWidth,

        SCREEN_HEIGHT = window.innerHeight;
        console.log(SCREEN_WIDTH);
    // prepare camera
    var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 1, FAR = 2000;
    this.camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
    this.scene.add(this.camera);
    this.camera.position.set(0, 100, 300);
    this.camera.lookAt(new THREE.Vector3(0,0,0));

    // prepare renderer
    this.renderer = new THREE.WebGLRenderer({ antialias:true });
    this.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    this.renderer.setClearColor(this.scene.fog.color);
    this.renderer.shadowMapEnabled = true;
    this.renderer.shadowMapSoft = true;

    // prepare container
    this.container = document.createElement('div');
    document.body.appendChild(this.container);
    this.container.appendChild(this.renderer.domElement);

    // events
    THREEx.WindowResize(this.renderer, this.camera);

    // prepare controls (OrbitControls)
    this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
    this.controls.target = new THREE.Vector3(0, 0, 10);
    this.controls.minDistance = 150;
    this.controls.maxDistance = 500;
    this.controls.maxPolarAngle = Math.PI/2.2; 

    // prepare clock
    this.clock = new THREE.Clock();


    // prepare stats
    // this.stats = new Stats();
    // this.stats.domElement.style.position = 'absolute';
    // this.stats.domElement.style.left = '50px';
    // this.stats.domElement.style.bottom = '50px';
    // this.stats.domElement.style.zIndex = 1;
    // this.container.appendChild( this.stats.domElement );

    // add ambient light

    this.scene.add(new THREE.AmbientLight(0x989cb3));
    var light;

    light = new THREE.DirectionalLight(0xdfebff, 1);
    light.position.set(300, 400, 10);
    light.position.multiplyScalar(1);

    light.castShadow = true;
    light.shadowCameraVisible = true;

    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;

    var d = 200;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;

    this.scene.add(light);




    // add simple ground
    var ground = new THREE.Mesh( new THREE.PlaneGeometry(1000, 1000, 10, 10), new THREE.MeshLambertMaterial('textures/land.jpg') );
    ground.receiveShadow = true;
    ground.position.set(0, 0, 0);
    ground.rotation.x = -Math.PI / 2;
    // this.scene.add(ground);


      var loaderland = new THREE.TextureLoader();
        loaderland.load( 'textures/land1.jpg', function ( texture ) {

          var geometry = new THREE.PlaneGeometry(2000, 2000, 10, 10);



          // var material = new THREE.MeshLambertMaterial( { map: texture, overdraw: 1, shininess: 0 } );
          var material = new THREE.MeshLambertMaterial( { color: 0xac8a5c } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.x = - Math.PI/2;
          mesh.rotation.z = Math.PI/3;
          mesh.receiveShadow = true;
          mesh.position.set(0, -0.1, 0);

          this.group.add( mesh );

        } );


      var loadersky = new THREE.TextureLoader();
        loadersky.load( 'textures/skyboxflip.jpg', function ( texture ) {

          var geometry = new THREE.SphereGeometry( -1000, 10, 10 );


          var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 1 } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.y = Math.PI;
          this.group.add( mesh );

        } );

    var map = THREE.ImageUtils.loadTexture( "textures/spriteA.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 7;
    sprite.position.y = 10;
    sprite.position.z = 12;
    sprite.scale.x = 10;
    sprite.scale.y = 10;
    sprite.scale.z = 10;
    this.scene.add( sprite );

    var map = THREE.ImageUtils.loadTexture( "textures/spriteB.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 0;
    sprite.position.y = 13;
    sprite.position.z = -30;
    sprite.scale.x = 10;
    sprite.scale.y = 10;
    sprite.scale.z = 10;
    this.scene.add( sprite );

    var map = THREE.ImageUtils.loadTexture( "textures/spriteC.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 40;
    sprite.position.y = 13;
    sprite.position.z = -46;
    sprite.scale.x = 10;
    sprite.scale.y = 10;
    sprite.scale.z = 10;
    this.scene.add( sprite );



    // load a model

    this.loadWall();
    this.loadCeilingBlack();
    this.loadCeilingFloor();
    this.loadDoor();
    this.loadFrame();
    this.loadFrameWhite();
    this.loadGlass();

    this.loadModelwater();
    this.loadModelroad();
  },

  loadWall: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -8;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI/4.8;
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -8;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI/4.8;
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -8;
      object.position.y = 0.5;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI/4.8;
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //LoadDoor

  loadDoor: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Door.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x75859e });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -8;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI/4.8;
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


  //LoadFrame

  loadFrame: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Frame.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x131313 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -8;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI/4.8;
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //LoadFrameWhite

 loadFrameWhite: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/FrameWhite.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -8;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI/4.8;
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //LoadGlass

  loadGlass: function() {




    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505, shininess: 10});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -8;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI/4.8;
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },



  loadModelwater: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/water.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x7dbeff });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -8;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI/4.8;
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);
    });
  },

  loadModelroad: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/road.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xefb378 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -8;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI/4.8;
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);
    });
  }

};

function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {

      var textures = [];

      for ( var i = 0; i < tilesNum; i ++ ) {

        textures[ i ] = new THREE.Texture();

      }

      var imageObj = new Image();

      imageObj.onload = function() {

        var canvas, context;
        var tileWidth = imageObj.height;

        for ( var i = 0; i < textures.length; i ++ ) {

          canvas = document.createElement( 'canvas' );
          context = canvas.getContext( '2d' );
          canvas.height = tileWidth;
          canvas.width = tileWidth;
          context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
          textures[ i ].image = canvas
          textures[ i ].needsUpdate = true;

        }

      };

      imageObj.src = atlasImgUrl;

      return textures;

    }

// Animate the scene
function animate() {
  requestAnimationFrame(animate);
  render();
  update();
}

// Update controls and stats
function update() {
  lesson6.controls.update(lesson6.clock.getDelta());
  lesson6.stats.update();
}

// Render the scene
function render() {
  if (lesson6.renderer) {
    lesson6.renderer.render(lesson6.scene, lesson6.camera);
  }
}

// Initialize lesson on page load
function initializeLesson() {
  lesson6.init();
  animate();
}

if (window.addEventListener)
  window.addEventListener('load', initializeLesson, false);
else if (window.attachEvent)
  window.attachEvent('onload', initializeLesson);
else window.onload = initializeLesson;
