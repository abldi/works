if ( ! Detector.webgl ) {
 Detector.addGetWebGLMessage()
 $('.showload')[0].style.display = 'none';
}
var controls, water, sphere, renderer, camera, scene;

var parameters = {
    width: 2000,
    height: 2000,
    widthSegments: 250,
    heightSegments: 250,
    depth: 1500,
    param: 4,
    filterparam: 1
};

var waterNormals;

var group;
var projector;
var particleMaterial;
var objects = [];
var lesson6 = {
  scene: null,
  camera: null,
  renderer: null,
  container: null,
  controls: null,
  clock: null,
  stats: null,

  init: function() { // Initialization

      THREE.DefaultLoadingManager.onProgress = function ( item, loaded, total ) {

          $('.loadingbarcolor')[0].style.width = Math.min(100, parseInt(100.0 * loaded / total)) + '%';

          console.log(Math.min(100, parseInt(100.0 * loaded / total)));

          if (Math.min(100, parseInt(100.0 * loaded / total)) == 100) {
              $('.showload')[0].style.display = 'none';
          }

          return Math.min(100, parseInt(100.0 * loaded / total));
          //console.log( item, loaded, total );
      };

    // create main scene
    this.scene = new THREE.Scene();
    this.scene.fog = new THREE.FogExp2(0xcce0ff, 0.0005);

    group = new THREE.Group();
       this.scene.add( group );

    var SCREEN_WIDTH = window.innerWidth,

        SCREEN_HEIGHT = window.innerHeight;
        console.log(SCREEN_WIDTH);
    // prepare camera
    var VIEW_ANGLE = 60, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 1, FAR = 6000;
    this.camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
    this.scene.add(this.camera);
    this.camera.position.set(600, 300, 450);
    this.camera.lookAt(new THREE.Vector3(0,0,0));

    // prepare renderer
    this.renderer = new THREE.WebGLRenderer({ antialias:true });
    this.renderer.setPixelRatio(window.devicePixelRatio);

    this.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    this.renderer.setClearColor(this.scene.fog.color);
    this.renderer.shadowMapEnabled = true;
    this.renderer.shadowMapSoft = true;

    // prepare container
    this.container = document.createElement('div');
    document.body.appendChild(this.container);
    this.container.appendChild(this.renderer.domElement);

    // events
    THREEx.WindowResize(this.renderer, this.camera);

    // prepare controls (OrbitControls)
    this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
    this.controls.target = new THREE.Vector3(0, 0, 10);
    this.controls.minDistance = 450;
    this.controls.maxDistance = 900;
    this.controls.maxPolarAngle = Math.PI/2.2; 

    // prepare clock
    this.clock = new THREE.Clock();



    // add ambient light

    this.scene.add(new THREE.AmbientLight(0xb8bee0));
    

    var light;
    light = new THREE.DirectionalLight(0xf8eac7, 0.7);
    light.position.set(500, 400, -500);
    light.position.multiplyScalar(1);

    light.castShadow = true;
    light.shadowCameraVisible = true;
    light.shadow.bias= -0.000001;

    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;

    var d = 600;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 2000;
    light.shadowDarkness = 0.5;

    this.scene.add(light);

    var lightL;

    lightL = new THREE.DirectionalLight(0xdfebff, 0.2);
    lightL.position.set(-500, 400, -500);
    lightL.position.multiplyScalar(1);

    lightL.castShadow = false;
    lightL.shadowCameraVisible = false;
    lightL.shadow.bias= -0.000001;
    this.scene.add(lightL);









    // add sprites
    // B1
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteB1.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 75;
    sprite.position.y = 50;
    sprite.position.z = -300;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );


    // B2
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteB2.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -90;
    sprite.position.y = 50;
    sprite.position.z = -300;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A1
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA1.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 80;
    sprite.position.y = 50;
    sprite.position.z = -180;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );
    // this.objects.push( sprite );

    // A2
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA2.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -90;
    sprite.position.y = 50;
    sprite.position.z = -180;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A3
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA3.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -270;
    sprite.position.y = 50;
    sprite.position.z = -180;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A4
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA4.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -440;
    sprite.position.y = 50;
    sprite.position.z = -180;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );



    // A5
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA5.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 80;
    sprite.position.y = 50;
    sprite.position.z = -50;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A6
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA6.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -90;
    sprite.position.y = 50;
    sprite.position.z = -50;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A7
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA7.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -270;
    sprite.position.y = 50;
    sprite.position.z = -50;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A8
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA8.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -440;
    sprite.position.y = 50;
    sprite.position.z = -50;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // C1
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteC1.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 210;
    sprite.position.y = 50;
    sprite.position.z = -230;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // C2
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteC2.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 210;
    sprite.position.y = 70;
    sprite.position.z = -70;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // C3
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteC3.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -570;
    sprite.position.y = 50;
    sprite.position.z = -200;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // C4
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteC4.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -570;
    sprite.position.y = 70;
    sprite.position.z = -40;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    document.addEventListener('mousedown', onDocumentMouseDown, false);


      var loadersky = new THREE.TextureLoader();
        loadersky.load( 'textures/skyboxflip.jpg', function ( texture ) {

          var geometry = new THREE.SphereGeometry( -5000, 10, 10 );


          var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 1 } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.y = Math.PI;
          this.group.add( mesh );

        } );



    // load a model
    //A
    this.loadWall();
    this.loadCeilingBlack();
    this.loadCeilingFloor();
    this.loadGlass();


    //B
    this.loadBFloor();
    this.loadBGlass();
    this.loadBGrass();

    this.loadBTiles();
    this.loadBWall();


    //C

    this.loadC1Wall();
    this.loadC1Glass();
    this.loadC1Grass();

    this.loadC2Wall();
    this.loadC2Glass();
    this.loadC2Grass();

 

    this.loadModelstreet();
    this.loadModelgreen();
    // this.loadModelroad();
    this.loadModelwater();
    this.loadModelfence();
    this.loadModelmarker();
    this.loadModelland();

    this.loadModelfarm1();
    this.loadModelfarm2();
    this.loadModelfarm3();
    
    this.loadModeltruckred();
    this.loadModeltruckgreen();
    this.loadModeltruckblue();



    window.addEventListener('resize', onWindowResize, false);
  },

//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C
//C------------------------------------------------------------------------------------------------------------------C


//C1------------------------------------------------------------------------------------------------------------------C1


  loadC1Wall: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1WallS.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      

        //C ----- 1
        var object1 = object.clone();
        object1.position.x = 195;
        object1.position.y = 0;
        object1.position.z = -140;
        object1.rotation.x = - Math.PI/2;
        object1.rotation.z = 0;//rotattion
        object1.scale.set(0.63, 0.63, 0.63);
        lesson6.scene.add(object1);

        //C ----- 2
        var object2 = object.clone();
        object2.position.x = -540;
        object2.position.y = 0;
        object2.position.z = -248;
        object2.rotation.x = - Math.PI/2;
        object2.rotation.z = Math.PI;//rotattion
        object2.scale.set(0.63, 0.63, 0.63);
        lesson6.scene.add(object2);

        //C ----- 3
        var object3 = object.clone();
        object3.position.x = -550;
        object3.position.y = 0;
        object3.position.z = 220;
        object3.rotation.x = - Math.PI/2;
        object3.rotation.z = Math.PI;//rotattion
        object3.scale.set(0.63, 0.63, 0.63);
        lesson6.scene.add(object3);

        //C ----- 4
        var object4 = object.clone();
        object4.position.x = 190;
        object4.position.y = 0;
        object4.position.z = 383;
        object4.rotation.x = - Math.PI/2;
        object4.rotation.z = 0;//rotattion
        object4.scale.set(0.57, 0.57, 0.57);
        lesson6.scene.add(object4);

        //C ----- 5
        var object5 = object.clone();
        object5.position.x = -5;
        object5.position.y = 0;
        object5.position.z = 310;
        object5.rotation.x = - Math.PI/2;
        object5.rotation.z = - Math.PI/2;//rotattion
        object5.scale.set(0.6, 0.63, 0.63);
        lesson6.scene.add(object5);

        //C ----- 6
        var object6 = object.clone();
        object6.position.x = -509;
        object6.position.y = 0;
        object6.position.z = 310;
        object6.rotation.x = - Math.PI/2;
        object6.rotation.z = - Math.PI/2;//rotattion
        object6.scale.set(0.63, 0.63, 0.63);
        lesson6.scene.add(object6);

    });
  },


  loadC1Glass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1GlassS.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0xb9a988, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //C ------ 1
      var object1 = object.clone();
      object1.position.x = 195;
      object1.position.y = 0;
      object1.position.z = -140;
      object1.rotation.x = - Math.PI/2;
      object1.rotation.z = 0;//rotattion
      object1.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object1);

      //C ------ 2
      var object2 = object.clone();
      object2.position.x = -540;
      object2.position.y = 0;
      object2.position.z = -248;
      object2.rotation.x = - Math.PI/2;
      object2.rotation.z = Math.PI;//rotattion
      object2.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object2);

      //C ------ 3
      var object3 = object.clone();
      object3.position.x = -550;
      object3.position.y = 0;
      object3.position.z = 220;
      object3.rotation.x = - Math.PI/2;
      object3.rotation.z = Math.PI;//rotattion
      object3.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object3);

      //C ------ 4
      var object4 = object.clone();
      object4.position.x = 190;
      object4.position.y = 0;
      object4.position.z = 383;
      object4.rotation.x = - Math.PI/2;
      object4.rotation.z = 0;//rotattion
      object4.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object4);

      //C ------ 5
      var object5 = object.clone();
      object5.position.x = -5;
      object5.position.y = 0;
      object5.position.z = 310;
      object5.rotation.x = - Math.PI/2;
      object5.rotation.z = - Math.PI/2;//rotattion
      object5.scale.set(0.6, 0.63, 0.63);
      lesson6.scene.add(object5);

      //C ------ 6
      var object6 = object.clone();
      object6.position.x = -509;
      object6.position.y = 0;
      object6.position.z = 310;
      object6.rotation.x = - Math.PI/2;
      object6.rotation.z = - Math.PI/2;//rotattion
      object6.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object6);

    });
  },

  loadC1Grass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //C ------ 1
      var object1 = object.clone();
      object1.position.x = 195;
      object1.position.y = 0;
      object1.position.z = -140;
      object1.rotation.x = - Math.PI/2;
      object1.rotation.z = 0;//rotattion
      object1.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object1);

      //C ------ 2
      var object2 = object.clone();
      object2.position.x = -540;
      object2.position.y = 0;
      object2.position.z = -248;
      object2.rotation.x = - Math.PI/2;
      object2.rotation.z = Math.PI;//rotattion
      object2.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object2);

      //C ------ 3
      var object3 = object.clone();
      object3.position.x = -550;
      object3.position.y = 0;
      object3.position.z = 220;
      object3.rotation.x = - Math.PI/2;
      object3.rotation.z = Math.PI;//rotattion
      object3.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object3);

      //C ------ 4
      var object4 = object.clone();
      object4.position.x = 190;
      object4.position.y = 0;
      object4.position.z = 383;
      object4.rotation.x = - Math.PI/2;
      object4.rotation.z = 0;//rotattion
      object4.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object4);

      //C ------ 5
      var object5 = object.clone();
      object5.position.x = -5;
      object5.position.y = 0;
      object5.position.z = 310;
      object5.rotation.x = - Math.PI/2;
      object5.rotation.z = - Math.PI/2;//rotattion
      object5.scale.set(0.6, 0.63, 0.63);
      lesson6.scene.add(object5);

      //C ------ 6
      var object6 = object.clone();
      object6.position.x = -509;
      object6.position.y = 0;
      object6.position.z = 310;
      object6.rotation.x = - Math.PI/2;
      object6.rotation.z = - Math.PI/2;//rotattion
      object6.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object6);
    });
  },





//C2------------------------------------------------------------------------------------------------------------------C2

  loadC2Wall: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2WallS.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //C ------ 1
      var object1 = object.clone();
      object1.position.x = 190;
      object1.position.y = 0;
      object1.position.z = -145;
      object1.rotation.x = - Math.PI/2;
      object1.rotation.z = 0;//rotattion
      object1.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object1);

      //C ------ 2
      var object2 = object.clone();
      object2.position.x = -540;
      object2.position.y = 0;
      object2.position.z = 63;
      object2.rotation.x = - Math.PI/2;
      object2.rotation.z = Math.PI;//rotattion
      object2.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object2);

      //C ------ 3
      var object3 = object.clone();
      object3.position.x = -540;
      object3.position.y = 0;
      object3.position.z = 230;
      object3.rotation.x = - Math.PI/2;
      object3.rotation.z = Math.PI;//rotattion
      object3.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object3);

      //C ------ 4
      var object4 = object.clone();
      object4.position.x = 180;
      object4.position.y = 0;
      object4.position.z = 99;
      object4.rotation.x = - Math.PI/2;
      object4.rotation.z = 0;//rotattion
      object4.scale.set(0.58, 0.58, 0.58);
      lesson6.scene.add(object4);

      //C ------ 5
      var object5 = object.clone();
      object5.position.x = -13;
      object5.position.y = 0;
      object5.position.z = 305;
      object5.rotation.x = - Math.PI/2;
      object5.rotation.z = - Math.PI/2;//rotattion
      object5.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object5);

      //C ------ 6
      var object6 = object.clone();
      object6.position.x = -183;
      object6.position.y = 0;
      object6.position.z = 305;
      object6.rotation.x = - Math.PI/2;
      object6.rotation.z = - Math.PI/2;//rotattion
      object6.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object6);

    });
  },

  loadC2Glass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2GlassS.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0xb9a988, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //C ------ 1
      var object1 = object.clone();
      object1.position.x = 190;
      object1.position.y = 0;
      object1.position.z = -145;
      object1.rotation.x = - Math.PI/2;
      object1.rotation.z = 0;//rotattion
      object1.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object1);

      //C ------ 2
      var object2 = object.clone();
      object2.position.x = -540;
      object2.position.y = 0;
      object2.position.z = 63;
      object2.rotation.x = - Math.PI/2;
      object2.rotation.z = Math.PI;//rotattion
      object2.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object2);

      //C ------ 3
      var object3 = object.clone();
      object3.position.x = -540;
      object3.position.y = 0;
      object3.position.z = 230;
      object3.rotation.x = - Math.PI/2;
      object3.rotation.z = Math.PI;//rotattion
      object3.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object3);

      //C ------ 4
      var object4 = object.clone();
      object4.position.x = 180;
      object4.position.y = 0;
      object4.position.z = 99;
      object4.rotation.x = - Math.PI/2;
      object4.rotation.z = 0;//rotattion
      object4.scale.set(0.58, 0.58, 0.58);
      lesson6.scene.add(object4);

      //C ------ 5
      var object5 = object.clone();
      object5.position.x = -13;
      object5.position.y = 0;
      object5.position.z = 305;
      object5.rotation.x = - Math.PI/2;
      object5.rotation.z = - Math.PI/2;//rotattion
      object5.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object5);

      //C ------ 6
      var object6 = object.clone();
      object6.position.x = -183;
      object6.position.y = 0;
      object6.position.z = 305;
      object6.rotation.x = - Math.PI/2;
      object6.rotation.z = - Math.PI/2;//rotattion
      object6.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object6);

    });
  },

  loadC2Grass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //C ------ 1
      var object1 = object.clone();
      object1.position.x = 190;
      object1.position.y = 0;
      object1.position.z = -145;
      object1.rotation.x = - Math.PI/2;
      object1.rotation.z = 0;//rotattion
      object1.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object1);

      //C ------ 2
      var object2 = object.clone();
      object2.position.x = -540;
      object2.position.y = 0;
      object2.position.z = 63;
      object2.rotation.x = - Math.PI/2;
      object2.rotation.z = Math.PI;//rotattion
      object2.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object2);

      //C ------ 3
      var object3 = object.clone();
      object3.position.x = -540;
      object3.position.y = 0;
      object3.position.z = 230;
      object3.rotation.x = - Math.PI/2;
      object3.rotation.z = Math.PI;//rotattion
      object3.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object3);

      //C ------ 4
      var object4 = object.clone();
      object4.position.x = 180;
      object4.position.y = 0;
      object4.position.z = 99;
      object4.rotation.x = - Math.PI/2;
      object4.rotation.z = 0;//rotattion
      object4.scale.set(0.58, 0.58, 0.58);
      lesson6.scene.add(object4);

      //C ------ 5
      var object5 = object.clone();
      object5.position.x = -13;
      object5.position.y = 0;
      object5.position.z = 305;
      object5.rotation.x = - Math.PI/2;
      object5.rotation.z = - Math.PI/2;//rotattion
      object5.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object5);

      //C ------ 6
      var object6 = object.clone();
      object6.position.x = -183;
      object6.position.y = 0;
      object6.position.z = 305;
      object6.rotation.x = - Math.PI/2;
      object6.rotation.z = - Math.PI/2;//rotattion
      object6.scale.set(0.63, 0.63, 0.63);
      lesson6.scene.add(object6);

    });
  },










//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B
//B------------------------------------------------------------------------------------------------------------------B





  //B-1

  loadBFloor: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -175;
      object.position.y = 0;
      object.position.z = -230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.85, 0.85, 0.85);
      lesson6.scene.add(object);

    });
  },





  loadBGrass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BGrass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -175;
      object.position.y = 0;
      object.position.z = -230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.85, 0.85, 0.85);
      lesson6.scene.add(object);

    });
  },



  loadBTiles: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BTiles.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xeeeeee, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -175;
      object.position.y = 0;
      object.position.z = -230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.85, 0.85, 0.85);
      lesson6.scene.add(object);

    });
  },


  loadBWall: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BWallS.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xcccccc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -175;
      object.position.y = 0;
      object.position.z = -230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.85, 0.85, 0.85);
      lesson6.scene.add(object);

    });
  },


  loadBGlass: function() {




    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BGlassS.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0xb9a988, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -175;
      object.position.y = 0;
      object.position.z = -230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.85, 0.85, 0.85);
      lesson6.scene.add(object);

    });
  },





//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A
//A------------------------------------------------------------------------------------------------------------------A



//A0------------------------------------------------------------------------------------------------------------------A0


  loadWall: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wallxs.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      
      //A ------1
      var object1 = object.clone();
      object1.position.x = -8;
      object1.position.y = 0;
      object1.position.z = 14;
      object1.rotation.x = - Math.PI/2;
      object1.rotation.z = 0; //Rotattion
      object1.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object1);

      //A ------2
      var object2 = object.clone();
      object2.position.x = -167;
      object2.position.y = 0;
      object2.position.z = 14;
      object2.rotation.x = - Math.PI/2;
      object2.rotation.z = 0; //Rotattion
      object2.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object2);

      //A ------3
      var object3 = object.clone();
      object3.position.x = -11;
      object3.position.y = 0;
      object3.position.z = -222;
      object3.rotation.x = - Math.PI/2;
      object3.rotation.z = Math.PI ; //Rotattion
      object3.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object3);

      //A ------4
      var object4 = object.clone();
      object4.position.x = 147;
      object4.position.y = 0;
      object4.position.z = -222;
      object4.rotation.x = - Math.PI/2;
      object4.rotation.z = Math.PI ; //Rotattion
      object4.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object4);

      //A ------5
      var object5 = object.clone();
      object5.position.x = -189;
      object5.position.y = 0;
      object5.position.z = -222;
      object5.rotation.x = - Math.PI/2;
      object5.rotation.z = Math.PI ; //Rotattion
      object5.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object5);

      //A ------6
      var object6 = object.clone();
      object6.position.x = -350;
      object6.position.y = 0;
      object6.position.z = -222;
      object6.rotation.x = - Math.PI/2;
      object6.rotation.z = Math.PI ; //Rotattion
      object6.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object6);

      //A ------7
      var object7 = object.clone();
      object7.position.x = -505;
      object7.position.y = 0;
      object7.position.z = 14;
      object7.rotation.x = - Math.PI/2;
      object7.rotation.z = 0; //Rotattion
      object7.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object7);

      //A ------8
      var object8 = object.clone();
      object8.position.x = -343;
      object8.position.y = 0;
      object8.position.z = 14;
      object8.rotation.x = - Math.PI/2;
      object8.rotation.z = 0; //Rotattion
      object8.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object8);

      //A ------9
      var object9 = object.clone();
      object9.position.x = -7;
      object9.position.y = 0;
      object9.position.z = 295;
      object9.rotation.x = - Math.PI/2;
      object9.rotation.z = 0; //Rotattion
      object9.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object9);

      //A ------10
      var object10 = object.clone();
      object10.position.x = 148;
      object10.position.y = 0;
      object10.position.z = 76;
      object10.rotation.x = - Math.PI/2;
      object10.rotation.z = Math.PI; //Rotattion
      object10.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object10);

      //A ------11
      var object11 = object.clone();
      object11.position.x = -12;
      object11.position.y = 0;
      object11.position.z = 76;
      object11.rotation.x = - Math.PI/2;
      object11.rotation.z = Math.PI; //Rotattion
      object11.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object11);

      //A ------12
      var object12 = object.clone();
      object12.position.x = -167;
      object12.position.y = 0;
      object12.position.z = 295;
      object12.rotation.x = - Math.PI/2;
      object12.rotation.z = 0; //Rotattion
      object12.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object12);

      //A ------13
      var object13 = object.clone();
      object13.position.x = -505;
      object13.position.y = 0;
      object13.position.z = 295;
      object13.rotation.x = - Math.PI/2;
      object13.rotation.z = 0; //Rotattion
      object13.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object13);

      //A ------14
      var object14 = object.clone();
      object14.position.x = -349;
      object14.position.y = 0;
      object14.position.z = 76;
      object14.rotation.x = - Math.PI/2;
      object14.rotation.z = Math.PI; //Rotattion
      object14.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object14);


      //A ------15
      var object15 = object.clone();
      object15.position.x = -189;
      object15.position.y = 0;
      object15.position.z = 76;
      object15.rotation.x = - Math.PI/2;
      object15.rotation.z = Math.PI; //Rotattion
      object15.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object15);

      //A ------16
      var object16 = object.clone();
      object16.position.x = -345;
      object16.position.y = 0;
      object16.position.z = 295;
      object16.rotation.x = - Math.PI/2;
      object16.rotation.z = 0; //Rotattion
      object16.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object16);

    });
  },

  //CeilingBlack

  loadCeilingBlack: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, specular: 0xf19900 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //A ------1
      var object1 = object.clone();
      object1.position.x = -8;
      object1.position.y = 0;
      object1.position.z = 14;
      object1.rotation.x = - Math.PI/2;
      object1.rotation.z = 0; //Rotattion
      object1.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object1);

      //A ------2
      var object2 = object.clone();
      object2.position.x = -167;
      object2.position.y = 0;
      object2.position.z = 14;
      object2.rotation.x = - Math.PI/2;
      object2.rotation.z = 0; //Rotattion
      object2.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object2);

      //A ------3
      var object3 = object.clone();
      object3.position.x = -11;
      object3.position.y = 0;
      object3.position.z = -222;
      object3.rotation.x = - Math.PI/2;
      object3.rotation.z = Math.PI ; //Rotattion
      object3.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object3);

      //A ------4
      var object4 = object.clone();
      object4.position.x = 147;
      object4.position.y = 0;
      object4.position.z = -222;
      object4.rotation.x = - Math.PI/2;
      object4.rotation.z = Math.PI ; //Rotattion
      object4.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object4);


      //A ------5
      var object5 = object.clone();
      object5.position.x = -189;
      object5.position.y = 0;
      object5.position.z = -222;
      object5.rotation.x = - Math.PI/2;
      object5.rotation.z = Math.PI ; //Rotattion
      object5.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object5);

      //A ------6
      var object6 = object.clone();
      object6.position.x = -350;
      object6.position.y = 0;
      object6.position.z = -222;
      object6.rotation.x = - Math.PI/2;
      object6.rotation.z = Math.PI ; //Rotattion
      object6.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object6);

      //A ------7
      var object7 = object.clone();
      object7.position.x = -505;
      object7.position.y = 0;
      object7.position.z = 14;
      object7.rotation.x = - Math.PI/2;
      object7.rotation.z = 0; //Rotattion
      object7.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object7);

      //A ------8
      var object8 = object.clone();
      object8.position.x = -343;
      object8.position.y = 0;
      object8.position.z = 14;
      object8.rotation.x = - Math.PI/2;
      object8.rotation.z = 0; //Rotattion
      object8.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object8);

      //A ------9
      var object9 = object.clone();
      object9.position.x = -7;
      object9.position.y = 0;
      object9.position.z = 295;
      object9.rotation.x = - Math.PI/2;
      object9.rotation.z = 0; //Rotattion
      object9.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object9);

      //A ------10
      var object10 = object.clone();
      object10.position.x = 148;
      object10.position.y = 0;
      object10.position.z = 76;
      object10.rotation.x = - Math.PI/2;
      object10.rotation.z = Math.PI; //Rotattion
      object10.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object10);

      //A ------11
      var object11 = object.clone();
      object11.position.x = -12;
      object11.position.y = 0;
      object11.position.z = 76;
      object11.rotation.x = - Math.PI/2;
      object11.rotation.z = Math.PI; //Rotattion
      object11.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object11);

      //A ------12
      var object12 = object.clone();
      object12.position.x = -167;
      object12.position.y = 0;
      object12.position.z = 295;
      object12.rotation.x = - Math.PI/2;
      object12.rotation.z = 0; //Rotattion
      object12.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object12);

      //A ------13
      var object13 = object.clone();
      object13.position.x = -505;
      object13.position.y = 0;
      object13.position.z = 295;
      object13.rotation.x = - Math.PI/2;
      object13.rotation.z = 0; //Rotattion
      object13.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object13);

      //A ------14
      var object14 = object.clone();
      object14.position.x = -349;
      object14.position.y = 0;
      object14.position.z = 76;
      object14.rotation.x = - Math.PI/2;
      object14.rotation.z = Math.PI; //Rotattion
      object14.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object14);

      //A ------15
      var object15 = object.clone();
      object15.position.x = -189;
      object15.position.y = 0;
      object15.position.z = 76;
      object15.rotation.x = - Math.PI/2;
      object15.rotation.z = Math.PI; //Rotattion
      object15.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object15);

      //A ------16
      var object16 = object.clone();
      object16.position.x = -345;
      object16.position.y = 0;
      object16.position.z = 295;
      object16.rotation.x = - Math.PI/2;
      object16.rotation.z = 0; //Rotattion
      object16.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object16);


    });
  },

    //CeilingFloor

  loadCeilingFloor: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //A ------1
      var object1 = object.clone();
      object1.position.x = -8;
      object1.position.y = 12;
      object1.position.z = 14;
      object1.rotation.x = - Math.PI/2;
      object1.rotation.z = 0; //Rotattion
      object1.scale.set(0.57, 0.57, 0.2);
      lesson6.scene.add(object1);

      //A ------2
      var object2 = object.clone();
      object2.position.x = -167;
      object2.position.y = 12;
      object2.position.z = 14;
      object2.rotation.x = - Math.PI/2;
      object2.rotation.z = 0; //Rotattion
      object2.scale.set(0.57, 0.57, 0.2);
      lesson6.scene.add(object2);

      //A ------3
      var object3 = object.clone();
      object3.position.x = -11;
      object3.position.y = 12;
      object3.position.z = -222;
      object3.rotation.x = - Math.PI/2;
      object3.rotation.z = Math.PI ; //Rotattion
      object3.scale.set(0.57, 0.57, 0.2);
      lesson6.scene.add(object3);

      //A ------4
      var object4 = object.clone();
      object4.position.x = 147;
      object4.position.y = 12;
      object4.position.z = -222;
      object4.rotation.x = - Math.PI/2;
      object4.rotation.z = Math.PI ; //Rotattion
      object4.scale.set(0.57, 0.57, 0.2);
      lesson6.scene.add(object4);

      //A ------5
      var object5 = object.clone();
      object5.position.x = -189;
      object5.position.y = 12;
      object5.position.z = -222;
      object5.rotation.x = - Math.PI/2;
      object5.rotation.z = Math.PI ; //Rotattion
      object5.scale.set(0.57, 0.57, 0.2);
      lesson6.scene.add(object5);

      //A ------6
      var object6 = object.clone();
      object6.position.x = -350;
      object6.position.y = 12;
      object6.position.z = -222;
      object6.rotation.x = - Math.PI/2;
      object6.rotation.z = Math.PI ; //Rotattion
      object6.scale.set(0.57, 0.57, 0.2);
      lesson6.scene.add(object6);

      //A ------7
      var object7 = object.clone();
      object7.position.x = -505;
      object7.position.y = 12;
      object7.position.z = 14;
      object7.rotation.x = - Math.PI/2;
      object7.rotation.z = 0; //Rotattion
      object7.scale.set(0.57, 0.57, 0.2);
      lesson6.scene.add(object7);


      //A ------8
      var object8 = object.clone();
      object8.position.x = -343;
      object8.position.y = 12;
      object8.position.z = 14;
      object8.rotation.x = - Math.PI/2;
      object8.rotation.z = 0; //Rotattion
      object8.scale.set(0.57, 0.57, 0.2);
      lesson6.scene.add(object8);

      //A ------9
      var object9 = object.clone();
      object9.position.x = -7;
      object9.position.y = 12;
      object9.position.z = 295;
      object9.rotation.x = - Math.PI/2;
      object9.rotation.z = 0; //Rotattion
      object9.scale.set(0.57, 0.53, 0.2);
      lesson6.scene.add(object9);

      //A ------10
      var object10 = object.clone();
      object10.position.x = 148;
      object10.position.y = 12;
      object10.position.z = 76;
      object10.rotation.x = - Math.PI/2;
      object10.rotation.z = Math.PI; //Rotattion
      object10.scale.set(0.57, 0.53, 0.2);
      lesson6.scene.add(object10);

      //A ------11
      var object11 = object.clone();
      object11.position.x = -12;
      object11.position.y = 12;
      object11.position.z = 76;
      object11.rotation.x = - Math.PI/2;
      object11.rotation.z = Math.PI; //Rotattion
      object11.scale.set(0.57, 0.53, 0.2);
      lesson6.scene.add(object11);

      //A ------12
      var object12 = object.clone();
      object12.position.x = -167;
      object12.position.y = 12;
      object12.position.z = 295;
      object12.rotation.x = - Math.PI/2;
      object12.rotation.z = 0; //Rotattion
      object12.scale.set(0.57, 0.53, 0.2);
      lesson6.scene.add(object12);

      //A ------13
      var object13 = object.clone();
      object13.position.x = -505;
      object13.position.y = 12;
      object13.position.z = 295;
      object13.rotation.x = - Math.PI/2;
      object13.rotation.z = 0; //Rotattion
      object13.scale.set(0.57, 0.53, 0.2);
      lesson6.scene.add(object13);

      //A ------14
      var object14 = object.clone();
      object14.position.x = -349;
      object14.position.y = 12;
      object14.position.z = 76;
      object14.rotation.x = - Math.PI/2;
      object14.rotation.z = Math.PI; //Rotattion
      object14.scale.set(0.57, 0.53, 0.2);
      lesson6.scene.add(object14);

      //A ------15
      var object15 = object.clone();
      object15.position.x = -189;
      object15.position.y = 12;
      object15.position.z = 76;
      object15.rotation.x = - Math.PI/2;
      object15.rotation.z = Math.PI; //Rotattion
      object15.scale.set(0.57, 0.53, 0.2);
      lesson6.scene.add(object15);

      //A ------16
      var object16 = object.clone();
      object16.position.x = -345;
      object16.position.y = 12;
      object16.position.z = 295;
      object16.rotation.x = - Math.PI/2;
      object16.rotation.z = 0; //Rotattion
      object16.scale.set(0.57, 0.53, 0.2);
      lesson6.scene.add(object16);



    });
  },

  
  //LoadGlass

  loadGlass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0xb9a988, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //A ------1
      var object1 = object.clone();
      object1.position.x = -8;
      object1.position.y = 0;
      object1.position.z = 14;
      object1.rotation.x = - Math.PI/2;
      object1.rotation.z = 0; //Rotattion
      object1.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object1);

      //A ------2
      var object2 = object.clone();
      object2.position.x = -167;
      object2.position.y = 0;
      object2.position.z = 14;
      object2.rotation.x = - Math.PI/2;
      object2.rotation.z = 0; //Rotattion
      object2.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object2);

      //A ------3
      var object3 = object.clone();
      object3.position.x = -11;
      object3.position.y = 0;
      object3.position.z = -222;
      object3.rotation.x = - Math.PI/2;
      object3.rotation.z = Math.PI ; //Rotattion
      object3.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object3);

      //A ------4
      var object4 = object.clone();
      object4.position.x = 147;
      object4.position.y = 0;
      object4.position.z = -222;
      object4.rotation.x = - Math.PI/2;
      object4.rotation.z = Math.PI ; //Rotattion
      object4.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object4);

      //A ------5
      var object5 = object.clone();
      object5.position.x = -189;
      object5.position.y = 0;
      object5.position.z = -222;
      object5.rotation.x = - Math.PI/2;
      object5.rotation.z = Math.PI ; //Rotattion
      object5.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object5);

      //A ------6
      var object6 = object.clone();
      object6.position.x = -350;
      object6.position.y = 0;
      object6.position.z = -222;
      object6.rotation.x = - Math.PI/2;
      object6.rotation.z = Math.PI ; //Rotattion
      object6.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object6);

      //A ------7
      var object7 = object.clone();
      object7.position.x = -505;
      object7.position.y = 0;
      object7.position.z = 14;
      object7.rotation.x = - Math.PI/2;
      object7.rotation.z = 0; //Rotattion
      object7.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object7);

      //A ------8
      var object8 = object.clone();
      object8.position.x = -343;
      object8.position.y = 0;
      object8.position.z = 14;
      object8.rotation.x = - Math.PI/2;
      object8.rotation.z = 0; //Rotattion
      object8.scale.set(0.57, 0.57, 0.57);
      lesson6.scene.add(object8);

      //A ------9
      var object9 = object.clone();
      object.position.x = -7;
      object.position.y = 0;
      object.position.z = 295;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object);

      //A ------10
      var object10 = object.clone();
      object10.position.x = 148;
      object10.position.y = 0;
      object10.position.z = 76;
      object10.rotation.x = - Math.PI/2;
      object10.rotation.z = Math.PI; //Rotattion
      object10.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object10);

      //A ------11
      var object11 = object.clone();
      object11.position.x = -12;
      object11.position.y = 0;
      object11.position.z = 76;
      object11.rotation.x = - Math.PI/2;
      object11.rotation.z = Math.PI; //Rotattion
      object11.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object11);

      //A ------12
      var object12 = object.clone();
      object12.position.x = -167;
      object12.position.y = 0;
      object12.position.z = 295;
      object12.rotation.x = - Math.PI/2;
      object12.rotation.z = 0; //Rotattion
      object12.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object12);

      //A ------13
      var object13 = object.clone();
      object13.position.x = -505;
      object13.position.y = 0;
      object13.position.z = 295;
      object13.rotation.x = - Math.PI/2;
      object13.rotation.z = 0; //Rotattion
      object13.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object13);

      //A ------14
      var object14 = object.clone();
      object14.position.x = -349;
      object14.position.y = 0;
      object14.position.z = 76;
      object14.rotation.x = - Math.PI/2;
      object14.rotation.z = Math.PI; //Rotattion
      object14.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object14);

      //A ------15
      var object15 = object.clone();
      object15.position.x = -189;
      object15.position.y = 0;
      object15.position.z = 76;
      object15.rotation.x = - Math.PI/2;
      object15.rotation.z = Math.PI; //Rotattion
      object15.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object15);

      //A ------16
      var object16 = object.clone();
      object16.position.x = -345;
      object16.position.y = 0;
      object16.position.z = 295;
      object16.rotation.x = - Math.PI/2;
      object16.rotation.z = 0; //Rotattion
      object16.scale.set(0.57, 0.53, 0.57);
      lesson6.scene.add(object16);

    });
  },





loadModelwater: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/water.obj', function(object, materials) {

var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0xa3b8d5, envMap: background } );
      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = -2;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },






  loadModelfence: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/fence.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xeeeeee });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 0;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },


  loadModelgreen: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/green.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x1c270e });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 0.5;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },



  loadModelstreet: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/Street.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x656565 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 0.3;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },

  loadModelmarker: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/marker.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xeeeeee });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 1;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },


  


  loadModelfarm1: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/farm1.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({ color: 0xaf9670 });
      var material2 = new THREE.MeshPhongMaterial( { color: 0xaf9670 } );


      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 1;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },


  loadModelfarm2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/farm2.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({ color: 0xa08a68 });
      var material2 = new THREE.MeshPhongMaterial( { color: 0xa08a68 } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 1;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },

  loadModelfarm3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/farm3.obj', function(object, materials) {


      var material2 = new THREE.MeshPhongMaterial( { color: 0xaa997e } );
      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 1;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },

  loadModeltruckred: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/redtruck.obj', function(object, materials) {


      var material2 = new THREE.MeshPhongMaterial( { color: 0x5e2832 } );
      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 1.5;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },

  loadModeltruckgreen: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/greentruck.obj', function(object, materials) {


      var material2 = new THREE.MeshPhongMaterial( { color: 0x31392b } );
      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 1.5;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },

  loadModeltruckblue: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/bluetruck.obj', function(object, materials) {


      var material2 = new THREE.MeshPhongMaterial( { color: 0x4e5974 } );
      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 1.5;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },


  loadModelland: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/land.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({ color: 0xaa997e });
      var material2 = new THREE.MeshPhongMaterial( { color: 0xbda380 } );
      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -168;
      object.position.y = 0;
      object.position.z = -50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.00129, 0.00129, 0.00129);
      lesson6.scene.add(object);
    });
  },


  loadModelroad: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/road.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xefb378 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -820;
      object.position.y = 3;
      object.position.z = 460;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(5, 5, 5);
      lesson6.scene.add(object);
    });
  }

};

function onWindowResize(){
  renderer.setSize(window.innerWidth, window.innerHeight);
}

function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {

      var textures = [];

      for ( var i = 0; i < tilesNum; i ++ ) {

        textures[ i ] = new THREE.Texture();

      }

      var imageObj = new Image();

      imageObj.onload = function() {

        var canvas, context;
        var tileWidth = imageObj.height;

        for ( var i = 0; i < textures.length; i ++ ) {

          canvas = document.createElement( 'canvas' );
          context = canvas.getContext( '2d' );
          canvas.height = tileWidth;
          canvas.width = tileWidth;
          context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
          textures[ i ].image = canvas
          textures[ i ].needsUpdate = true;

        }

      };

      imageObj.src = atlasImgUrl;

      return textures;

    }

    function onDocumentMouseDown(event) {
        event.preventDefault();
        console.log("get")
        var vector = new THREE.Vector3((event.clientX / window.innerWidth) * 2 -
            1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
        vector.unproject(lesson6.camera);


        var raycaster = new THREE.Raycaster(lesson6.camera.position, vector.sub(lesson6.camera.position)
            .normalize());
        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {
           window.open(intersects[0].object.userData.URL, "_self");
        }
      }





// Animate the scene
function animate() {
  requestAnimationFrame(animate);
  render();
  update();
}

// Update controls and stats
function update() {
  lesson6.controls.update(lesson6.clock.getDelta());
}

 var time;

// Render the scene
function render() {

    lesson6.renderer.render(lesson6.scene, lesson6.camera);

}

// Initialize lesson on page load
function initializeLesson() {
  lesson6.init();
  animate();
}

if (window.addEventListener)
  window.addEventListener('load', initializeLesson, false);
else if (window.attachEvent)
  window.attachEvent('onload', initializeLesson);
else window.onload = initializeLesson;
