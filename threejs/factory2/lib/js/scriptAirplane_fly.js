if ( ! Detector.webgl ) {
 Detector.addGetWebGLMessage()
 $('.showload')[0].style.display = 'none';
}
var group;
var camera;
var cam1 = new THREE.PerspectiveCamera( 80, window.innerWidth / window.innerHeight, 0.1, 1000 );
var cam2 = new THREE.PerspectiveCamera( 80, window.innerWidth / window.innerHeight, 0.1, 1000 );

var con1 = new THREE.OrbitControls( cam1 );
var con2 = new THREE.OrbitControls( cam2 );


var pipeSpline = [
        new THREE.Vector3( 310, 150, 210 ),
        new THREE.Vector3( 270, 20, 130 ),
        new THREE.Vector3( 250, 15, -20 ),
        new THREE.Vector3( 130, 15, -20 ),
        new THREE.Vector3( -100, 15, 100 ),
        new THREE.Vector3( 225, 5, -30 ),
        new THREE.Vector3( 50, 5, -30 ),
        new THREE.Vector3( -200, 5, -30 ),
        new THREE.Vector3( -300, 5, -70 )
    ]

var pipeSpline2 = [
        new THREE.Vector3( 130, 0, 0 ),
        new THREE.Vector3( 100, 0, -10 ),
        new THREE.Vector3( 80, 0, -20 ),
        new THREE.Vector3( 40, 0, -20 ),
        new THREE.Vector3( 0, 0, -30 ),
        new THREE.Vector3( 0, 0, -30 ),
        new THREE.Vector3( -200, 0, -30 ),
        new THREE.Vector3( -200, 0, -30 ),
        new THREE.Vector3( -100, 0, 50 )
    ]

var spline = new THREE.SplineCurve3(pipeSpline);
var spline2 = new THREE.SplineCurve3(pipeSpline2);
var camPosIndex = 0;

// var scene = null;
// // var camera = null;
// var renderer = null;
// var container = null;
// var controls = null;
// var clock = null;
// var stats = null;




  function init() { // Initialization

    THREE.DefaultLoadingManager.onProgress = function ( item, loaded, total ) {

          $('.loadingbarcolor')[0].style.width = Math.min(100, parseInt(100.0 * loaded / total)) + '%';

          console.log(Math.min(100, parseInt(100.0 * loaded / total)));

          if (Math.min(100, parseInt(100.0 * loaded / total)) == 100) {
              $('.showload')[0].style.display = 'none';
          }

          return Math.min(100, parseInt(100.0 * loaded / total));
          //console.log( item, loaded, total );
      };

    // create main scene
    scene = new THREE.Scene();
    scene.fog = new THREE.FogExp2(0xcce0ff, 0.0018);


    







    group = new THREE.Group();
       scene.add( group );

    var SCREEN_WIDTH = window.innerWidth,

        SCREEN_HEIGHT = window.innerHeight;
        console.log(SCREEN_WIDTH);
    // prepare camera
    var VIEW_ANGLE = 60, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 1, FAR = 2000;
    camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
    scene.add(camera);
    camera.position.set(200, 100, 300);
    // camera.lookAt(new THREE.Vector3(0,0,0));



    // prepare renderer
    renderer = new THREE.WebGLRenderer({ antialias:true });

    renderer.setPixelRatio(window.devicePixelRatio);

    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    renderer.setClearColor(scene.fog.color);
    renderer.shadowMapEnabled = true;
    renderer.shadowMapSoft = true;

    // prepare container
    container = document.createElement('div');
    document.body.appendChild(container);
    container.appendChild(renderer.domElement);

    // events
    THREEx.WindowResize(renderer, camera);

    // prepare controls (OrbitControls)
    // controls = new THREE.OrbitControls(camera, renderer.domElement);
    // controls.target = new THREE.Vector3(0, 0, 0);
    // controls.minDistance = 50;
    // controls.maxDistance = 400;
    // controls.maxPolarAngle = Math.PI/2.05; 

    // controls.center.set(0, 0, 0);
    // camera.position.copy(controls.center).add(new THREE.Vector3(100, 100, 100));

    // prepare clock
    clock = new THREE.Clock();


    scene.add(new THREE.AmbientLight(0xb8bee0));

    var light;

    light = new THREE.DirectionalLight(0xf8eac7, 0.7);
    light.position.set(-200, 400, 300);
    light.position.multiplyScalar(1);

    light.castShadow = true;
    light.shadowCameraVisible = true;

    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;

    var d = 240;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;

    scene.add(light);



    var lightL;

    lightL = new THREE.DirectionalLight(0xdfebff, 0.3);
    lightL.position.set(-200, 400, -300);
    lightL.position.multiplyScalar(1);

    lightL.castShadow = false;
    lightL.shadowCameraVisible = false;
    scene.add(lightL);



    // add simple ground
    var ground = new THREE.Mesh( new THREE.PlaneGeometry(20, 20, 10, 10), new THREE.MeshPhongMaterial({color: 0xff0000}) );
    ground.position.set(0, 10, 0);
    ground.rotation.x = -Math.PI / 2;
    scene.add(ground);

    var ground = new THREE.Mesh( new THREE.PlaneGeometry(20, 20, 10, 10), new THREE.MeshPhongMaterial({color: 0xff0000}) );
    ground.position.set(0, 10, 100);
    ground.rotation.x = -Math.PI / 2;
    scene.add(ground);


      var loaderland = new THREE.TextureLoader();
        loaderland.load( 'textures/land1.png', function ( texture ) {

          var geometry = new THREE.PlaneGeometry(2000, 2000, 10, 10);



          // var material = new THREE.MeshPhongMaterial( { map: texture, overdraw: 1, shininess: 0 } );
          var material = new THREE.MeshPhongMaterial( { color: 0x597720 } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.x = - Math.PI/2;
          mesh.rotation.z = Math.PI/3;
          mesh.receiveShadow = true;
          mesh.position.set(0, -0.2, 0);

          group.add( mesh );

        } );


      var loadersky = new THREE.TextureLoader();
        loadersky.load( 'textures/skyboxflip.jpg', function ( texture ) {

          var geometry = new THREE.SphereGeometry( -1000, 10, 10 );


          var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 1 } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.y = Math.PI;
          group.add( mesh );

        } );


    // loadModelbuilding();
    loadModelwall();
    loadModelwindow();
    loadModelH();
    loadModelbase();
    loadModelceiling();
    loadModelceilingglass();
    loadModelfloor();
    loadModelglass();
    loadModelstreet();
    loadModelgreen();
    loadModelwingwindow();
    loadModelwingbody();
    loadModelwingceiling();
    loadModeloutsidefloor();
    loadModelwingframe();
    loadModelsidebuilding();
    loadModelcarslot();
  }


  function setupControls() {
            // cam1 = new THREE.PerspectiveCamera( 80, window.innerWidth / window.innerHeight, 0.1, 1000 );
        cam1.position.set( 0, 0, -0.1 );
        // con1 = new THREE.OrbitControls( cam1 );


            // cam2 = new THREE.PerspectiveCamera( 80, window.innerWidth / window.innerHeight, 0.1, 1000 );
        cam2.position.set( 0, 0, -0.1 );
        // con2 = new THREE.OrbitControls( cam2 );
        }



      function setControlsDevice() {
            camera = cam1;
            controls = con1;
            controls.enableZoom = false;
        controls.enablePan = false;
           

      }

      function setControlsOrbit() {
            camera = cam2;
            controls = con2;
            controls.enableZoom = false;
        controls.enablePan = false;
      }



  function loadModelcarslot()  {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/carslot.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x527318 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0.1;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelsidebuilding() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/sidebuilding.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({color: 0x2d6ac5, transparent: true, opacity: 0.4});

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = false;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelwingwindow() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/wingwindow.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505, shininess: 10});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x66b8f7, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = false;


        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);

    });
  }
  function loadModelwingframe() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/wingframe.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x333333 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelwingceiling() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/wingceiling.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelwingbody(){

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/wingbody.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModeloutsidefloor() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/outsidefloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }

  function loadModelfloor() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/floor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelbase() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/base.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelceilingglass() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/ceilingglass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505, shininess: 10});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x66b8f7, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = false;


        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);

    });
  }
  function loadModelceiling() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/ceiling.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }
  function loadModelgreen() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/green.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x3a6331 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }

  function loadModelH() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/H.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x343434 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }

  function loadModelwall() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xe3e3e3 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;
          child.material.side = THREE.DoubleSide;


          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }

   function loadModelwindow() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/window.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x333333 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }


  function loadModelglass() {




    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505, shininess: 10});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x4199ac, envMap: background } );


      object.traverse( function( node ) {
          if( node.material ) {
              node.material.side = THREE.DoubleSide;
          }
      });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = false;
          child.receiveShadow = false;


        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);

    });
  }

  function loadModelstreet() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/airplane/street.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x818181 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      scene.add(object);
    });
  }



function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {

      var textures = [];

      for ( var i = 0; i < tilesNum; i ++ ) {

        textures[ i ] = new THREE.Texture();

      }

      var imageObj = new Image();

      imageObj.onload = function() {

        var canvas, context;
        var tileWidth = imageObj.height;

        for ( var i = 0; i < textures.length; i ++ ) {

          canvas = document.createElement( 'canvas' );
          context = canvas.getContext( '2d' );
          canvas.height = tileWidth;
          canvas.width = tileWidth;
          context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
          textures[ i ].image = canvas
          textures[ i ].needsUpdate = true;

        }

      };

      imageObj.src = atlasImgUrl;

      return textures;

    }

// Animate the scene
function animate() {
  requestAnimationFrame(animate);
  render();
  update();
}

// Update controls and stats
function update() {


  
  camPosIndex++;
          if (camPosIndex > 10000) {
            camPosIndex = 0;
          }
          // console.error(camPosIndex);
          var camPos = spline.getPoint(camPosIndex / 1000);
          var camRot = spline.getTangent(camPosIndex / 1000);

          cam1.position.x = camPos.x;
          cam1.position.y = camPos.y;
          cam1.position.z = camPos.z;
          
          cam1.rotation.x = camRot.x;
          cam1.rotation.y = camRot.y;
          cam1.rotation.z = camRot.z;
          
          cam1.lookAt(spline2.getPoint(camPosIndex / 1000));


  // console.error(camera.position);
  // console.error(camera.lookAt.position);
  

  // console.error(camera.position) ;



  con1.update(clock.getDelta());
}


// Render the scene
function render() {
  if (renderer) {
    renderer.render(scene, camera);
  }
}

// Initialize lesson on page load
function initializeLesson() {
  init();
  animate();
}

if (window.addEventListener)
  window.addEventListener('load', initializeLesson, false);
else if (window.attachEvent)
  window.attachEvent('onload', initializeLesson);
else window.onload = initializeLesson;
