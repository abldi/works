/**
 *
 * WebGL With Three.js - Lesson 6 - loading models
 * http://www.script-tutorials.com/webgl-with-three-js-lesson-6/
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Script Tutorials
 * http://www.script-tutorials.com/
 */
var group;
var lesson6 = {
  scene: null,
  camera: null,
  renderer: null,
  container: null,
  controls: null,
  clock: null,
  stats: null,







  init: function() { // Initialization



    // create main scene
    this.scene = new THREE.Scene();
    this.scene.fog = new THREE.FogExp2(0xcce0ff, 0.00015);



    group = new THREE.Group();
       this.scene.add( group );

    var SCREEN_WIDTH = window.innerWidth,

        SCREEN_HEIGHT = window.innerHeight;
        console.log(SCREEN_WIDTH);
    // prepare camera
    var VIEW_ANGLE = 70, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 1, FAR = 2000;
    this.camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
    this.scene.add(this.camera);
    this.camera.position.set(0, 0, 0);
    this.camera.lookAt(new THREE.Vector3(0,0,0));

    // prepare renderer
    this.renderer = new THREE.WebGLRenderer({ antialias:true });
    this.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    this.renderer.setClearColor(this.scene.fog.color);
    this.renderer.shadowMapEnabled = true;
    this.renderer.shadowMapSoft = true;

    // prepare container
    this.container = document.createElement('div');
    document.body.appendChild(this.container);
    this.container.appendChild(this.renderer.domElement);

    // events
    THREEx.WindowResize(this.renderer, this.camera);

    // prepare controls (OrbitControls)
    this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
    this.controls.target = new THREE.Vector3(0, 0, 10);
    this.controls.minDistance = 100;
    this.controls.maxDistance = 400;
    // this.controls.maxPolarAngle = Math.PI/2.2; 

    // prepare clock
    this.clock = new THREE.Clock();


    // prepare stats
    // this.stats = new Stats();
    // this.stats.domElement.style.position = 'absolute';
    // this.stats.domElement.style.left = '50px';
    // this.stats.domElement.style.bottom = '50px';
    // this.stats.domElement.style.zIndex = 1;
    // this.container.appendChild( this.stats.domElement );

    // add ambient light

    this.scene.add(new THREE.AmbientLight(0x616fb4));
    var light;

    light = new THREE.DirectionalLight(0xdfebff, 1);
    light.position.set(300, 400, 10);
    light.position.multiplyScalar(1);

    light.castShadow = true;
    light.shadowCameraVisible = true;

    light.shadowMapWidth = 2048;
    light.shadowMapHeight = 2048;

    var d = 200;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;

    this.scene.add(light);




    // add simple ground
    var ground = new THREE.Mesh( new THREE.PlaneGeometry(1000, 1000, 10, 10), new THREE.MeshLambertMaterial('textures/land.jpg') );
    ground.receiveShadow = true;
    ground.position.set(0, 0, 0);
    ground.rotation.x = -Math.PI / 2;
    // this.scene.add(ground);





    var loadersky = new THREE.TextureLoader();
      loadersky.load( 'textures/factorystreet6.jpg', function ( texture ) {

        var geometry = new THREE.SphereGeometry( -1000, 70, 70 );


        var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 1 } );
        var mesh = new THREE.Mesh( geometry, material );
        mesh.rotation.y = Math.PI;
        this.group.add( mesh );

      } );

    var materialline = new THREE.LineBasicMaterial({ color: 0x91dcff });

    var geometry = new THREE.Geometry();
    geometry.vertices.push(
      new THREE.Vector3( 300, 10, 990 ),
      new THREE.Vector3( 300, -10, 990 ),
      new THREE.Vector3( -300, -10, 990 ),
      new THREE.Vector3( -300, 10, 990 )
    );

    var line = new THREE.Line( geometry, materialline );
    this.scene.add( line );




      

    var map = THREE.ImageUtils.loadTexture( "textures/spriteA.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    var x = 0;
    var y = 60;
    var z = 950;
    var xS = 0;
    var yS = 60;
    var zS = 950;
SnapToSphere(x , y , z);
    sprite.position.x = xS;
    sprite.position.y = yS;
    sprite.position.z = zS;
    sprite.scale.x = 100;
    sprite.scale.y = 100;
    sprite.scale.z = 100;
    this.scene.add( sprite );

    var map = THREE.ImageUtils.loadTexture( "textures/spriteB.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -950;
    sprite.position.y = 40;
    sprite.position.z = 0;
    sprite.scale.x = 100;
    sprite.scale.y = 100;
    sprite.scale.z = 100;
    this.scene.add( sprite );

    var map = THREE.ImageUtils.loadTexture( "textures/FB.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -920;
    sprite.position.y = 48;
    sprite.position.z = -130;
    sprite.scale.x = 200;
    sprite.scale.y = 50;
    sprite.scale.z = 100;
    this.scene.add( sprite );

    var map = THREE.ImageUtils.loadTexture( "textures/spriteC.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 950;
    sprite.position.y = 180;
    sprite.position.z = 0;
    sprite.scale.x = 100;
    sprite.scale.y = 100;
    sprite.scale.z = 100;
    this.scene.add( sprite );



    // load a model
    this.loadModel();

  },

  loadModel: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/factory.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x82a8c9 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -8;
      object.position.y = 0;
      object.position.z = 90;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI/4.8;
      object.scale.set(0.6, 0.6, 0.6);
      // lesson6.scene.add(object);
    });
  }

};




function SnapToSphere( x , y , z , xS , yS , zS ){
    var d = Math.sqrt(x*x + y*y + z*z);
    var xS = x * 1100 / d;
    var yS = y * 1100 / d;
    var zS = z * 1100 / d;

}



function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {

      var textures = [];

      for ( var i = 0; i < tilesNum; i ++ ) {

        textures[ i ] = new THREE.Texture();

      }

      var imageObj = new Image();

      imageObj.onload = function() {

        var canvas, context;
        var tileWidth = imageObj.height;

        for ( var i = 0; i < textures.length; i ++ ) {

          canvas = document.createElement( 'canvas' );
          context = canvas.getContext( '2d' );
          canvas.height = tileWidth;
          canvas.width = tileWidth;
          context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
          textures[ i ].image = canvas
          textures[ i ].needsUpdate = true;

        }

      };

      imageObj.src = atlasImgUrl;

      return textures;

    }

// Animate the scene
function animate() {
  requestAnimationFrame(animate);
  render();
  update();
}

// Update controls and stats
function update() {
  lesson6.controls.update(lesson6.clock.getDelta());
  lesson6.stats.update();
}

// Render the scene
function render() {
  if (lesson6.renderer) {
    lesson6.renderer.render(lesson6.scene, lesson6.camera);
  }
}

// Initialize lesson on page load
function initializeLesson() {
  lesson6.init();
  animate();
}

if (window.addEventListener)
  window.addEventListener('load', initializeLesson, false);
else if (window.attachEvent)
  window.attachEvent('onload', initializeLesson);
else window.onload = initializeLesson;
