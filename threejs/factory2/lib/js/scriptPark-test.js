

var group;
var projector;
var particleMaterial;
var objects = [];
var lesson6 = {
  scene: null,
  camera: null,
  renderer: null,
  container: null,
  controls: null,
  clock: null,
  stats: null,

  init: function() { // Initialization



    // create main scene
    this.scene = new THREE.Scene();
    this.scene.fog = new THREE.FogExp2(0xcce0ff, 0.0006);

    group = new THREE.Group();
       this.scene.add( group );

    var SCREEN_WIDTH = window.innerWidth,

        SCREEN_HEIGHT = window.innerHeight;
        console.log(SCREEN_WIDTH);
    // prepare camera
    var VIEW_ANGLE = 60, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 1, FAR = 4000;
    this.camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
    this.scene.add(this.camera);
    this.camera.position.set(0, 100, -0);
    this.camera.lookAt(new THREE.Vector3(0,0,0));

    // prepare renderer
    this.renderer = new THREE.WebGLRenderer({ antialias:true });
    this.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    this.renderer.setClearColor(this.scene.fog.color);
    this.renderer.shadowMapEnabled = true;
    this.renderer.shadowMapSoft = true;

    // prepare container
    this.container = document.createElement('div');
    document.body.appendChild(this.container);
    this.container.appendChild(this.renderer.domElement);

    // events
    THREEx.WindowResize(this.renderer, this.camera);

    // prepare controls (OrbitControls)
    this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
    this.controls.target = new THREE.Vector3(0, 0, 10);
    this.controls.minDistance = 300;
    this.controls.maxDistance = 1200;
    this.controls.maxPolarAngle = Math.PI/2.2; 

    // prepare clock
    this.clock = new THREE.Clock();



    // add ambient light

    this.scene.add(new THREE.AmbientLight(0x989cb3));
    var light;

    light = new THREE.DirectionalLight(0xdfebff, 1);
    light.position.set(200, 400, 200);
    light.position.multiplyScalar(1);

    light.castShadow = true;
    light.shadowCameraVisible = true;

    light.shadowMapWidth = 4096;
    light.shadowMapHeight = 4096;

    var d = 750;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.5;

    this.scene.add(light);

    // add sprites
    // B1
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteB1.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 80;
    sprite.position.y = 50;
    sprite.position.z = -350;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );







    // B2
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteB2.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -90;
    sprite.position.y = 50;
    sprite.position.z = -350;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A1
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA1.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.userData = { URL: "http://www.langzou.xyz/projects/factory/index.html"};
    sprite.position.x = 80;
    sprite.position.y = 50;
    sprite.position.z = -200;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );
    this.objects.push( sprite );

    // A2
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA2.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -90;
    sprite.position.y = 50;
    sprite.position.z = -200;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A3
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA3.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -270;
    sprite.position.y = 50;
    sprite.position.z = -200;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A4
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA4.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -440;
    sprite.position.y = 50;
    sprite.position.z = -200;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );



    // A5
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA5.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 80;
    sprite.position.y = 50;
    sprite.position.z = -70;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A6
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA6.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -90;
    sprite.position.y = 50;
    sprite.position.z = -70;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A7
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA7.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -270;
    sprite.position.y = 50;
    sprite.position.z = -70;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // A8
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteA8.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -440;
    sprite.position.y = 50;
    sprite.position.z = -70;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // C1
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteC1.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 210;
    sprite.position.y = 50;
    sprite.position.z = -240;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // C2
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteC2.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = 210;
    sprite.position.y = 70;
    sprite.position.z = -70;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // C3
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteC3.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -570;
    sprite.position.y = 50;
    sprite.position.z = -240;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );

    // C4
    var map = THREE.ImageUtils.loadTexture( "textures/sprites/spriteC4.png" );
    var materialsprite = new THREE.SpriteMaterial( { map: map, fog: true } );
    var sprite = new THREE.Sprite( materialsprite );
    sprite.position.x = -570;
    sprite.position.y = 70;
    sprite.position.z = -70;
    sprite.scale.x = 50;
    sprite.scale.y = 50;
    sprite.scale.z = 50;
    this.scene.add( sprite );



    // add simple ground
    var ground = new THREE.Mesh( new THREE.PlaneGeometry(5000, 5000, 10, 10), new THREE.MeshLambertMaterial('textures/land.jpg') );
    ground.receiveShadow = true;
    ground.position.set(0, 0, 0);
    ground.rotation.x = -Math.PI / 2;
    // this.scene.add(ground);


      var loaderland = new THREE.TextureLoader();
        loaderland.load( 'textures/land1.jpg', function ( texture ) {

          var geometry = new THREE.PlaneGeometry(5000, 5000, 10, 10);



          // var material = new THREE.MeshLambertMaterial( { map: texture, overdraw: 1, shininess: 0 } );
          var material = new THREE.MeshLambertMaterial( { color: 0xac8a5c } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.x = - Math.PI/2;
          mesh.rotation.z = Math.PI/3;
          mesh.receiveShadow = true;
          mesh.position.set(0, -0.1, 0);

          this.group.add( mesh );

        } );


      var loadersky = new THREE.TextureLoader();
        loadersky.load( 'textures/skyboxflip.jpg', function ( texture ) {

          var geometry = new THREE.SphereGeometry( -5000, 10, 10 );


          var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 1 } );
          var mesh = new THREE.Mesh( geometry, material );
          mesh.rotation.y = Math.PI;
          this.group.add( mesh );

        } );

    


    // load a model
    //A
    this.loadWall();
    this.loadCeilingBlack();
    this.loadCeilingFloor();
    this.loadGlass();

    this.loadWall1();
    this.loadCeilingBlack1();
    this.loadCeilingFloor1();
    this.loadGlass1();

    this.loadWall2();
    this.loadCeilingBlack2();
    this.loadCeilingFloor2();
    this.loadGlass2();

    this.loadWall3();
    this.loadCeilingBlack3();
    this.loadCeilingFloor3();
    this.loadGlass3();


    this.loadWall4();
    this.loadCeilingBlack4();
    this.loadCeilingFloor4();
    this.loadGlass4();

    this.loadWall5();
    this.loadCeilingBlack5();
    this.loadCeilingFloor5();
    this.loadGlass5();

    this.loadWall6();
    this.loadCeilingBlack6();
    this.loadCeilingFloor6();
    this.loadGlass6();

    this.loadWall7();
    this.loadCeilingBlack7();
    this.loadCeilingFloor7();
    this.loadGlass7();

    this.loadWall8();
    this.loadCeilingBlack8();
    this.loadCeilingFloor8();
    this.loadGlass8();

    this.loadWall9();
    this.loadCeilingBlack9();
    this.loadCeilingFloor9();
    this.loadGlass9();

    this.loadWall10();
    this.loadCeilingBlack10();
    this.loadCeilingFloor10();
    this.loadGlass10();

    this.loadWall11();
    this.loadCeilingBlack11();
    this.loadCeilingFloor11();
    this.loadGlass11();

    this.loadWall12();
    this.loadCeilingBlack12();
    this.loadCeilingFloor12();
    this.loadGlass12();

    this.loadWall13();
    this.loadCeilingBlack13();
    this.loadCeilingFloor13();
    this.loadGlass13();

    this.loadWall14();
    this.loadCeilingBlack14();
    this.loadCeilingFloor14();
    this.loadGlass14();

    this.loadWall15();
    this.loadCeilingBlack15();
    this.loadCeilingFloor15();
    this.loadGlass15();

    //B
    this.loadBFloor();
    this.loadBGlass();
    this.loadBGrass();

    this.loadBTiles();
    this.loadBWall();


    //C

    this.loadC1Wall();
    this.loadC1Glass();
    this.loadC1Grass();

    this.loadC2Wall();
    this.loadC2Glass();
    this.loadC2Grass();

    this.loadC1Wall2();
    this.loadC1Glass2();
    this.loadC1Grass2();

    this.loadC2Wall2();
    this.loadC2Glass2();
    this.loadC2Grass2();

    this.loadC1Wall3();
    this.loadC1Glass3();
    this.loadC1Grass3();

    this.loadC2Wall3();
    this.loadC2Glass3();
    this.loadC2Grass3();

    this.loadC1Wall4();
    this.loadC1Glass4();
    this.loadC1Grass4();

    this.loadC2Wall4();
    this.loadC2Glass4();
    this.loadC2Grass4();


    this.loadC1Wall5();
    this.loadC1Glass5();
    this.loadC1Grass5();

    this.loadC2Wall5();
    this.loadC2Glass5();
    this.loadC2Grass5();

    this.loadC1Wall6();
    this.loadC1Glass6();
    this.loadC1Grass6();

    this.loadC2Wall6();
    this.loadC2Glass6();
    this.loadC2Grass6();

    this.loadModelroad();
    this.loadModelwater();

  },


  //C-1


  loadC1Wall: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 190;
      object.position.y = 0;
      object.position.z = -160;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


  loadC1Glass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 190;
      object.position.y = 0;
      object.position.z = -160;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC1Grass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 190;
      object.position.y = 0;
      object.position.z = -160;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },





//C2

  loadC2Wall: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 190;
      object.position.y = 0;
      object.position.z = -160;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },








  loadC2Glass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 190;
      object.position.y = 0;
      object.position.z = -160;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC2Grass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 190;
      object.position.y = 0;
      object.position.z = -160;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },




 //C-2


  loadC1Wall2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = -310;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


  loadC1Glass2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = -310;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC1Grass2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = -310;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },





//C2-2

  loadC2Wall2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = -10;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


  loadC2Glass2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = -10;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC2Grass2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = -10;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //C3
  loadC1Wall3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = 230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


  loadC1Glass3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = 230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC1Grass3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = 230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },





//C2

  loadC2Wall3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = 230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },








  loadC2Glass3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = 230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC2Grass3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -560;
      object.position.y = 0;
      object.position.z = 230;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },



  //C4



  loadC1Wall4: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 200;
      object.position.y = 0;
      object.position.z = 370;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


  loadC1Glass4: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 200;
      object.position.y = 0;
      object.position.z = 370;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC1Grass4: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 200;
      object.position.y = 0;
      object.position.z = 370;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },





//C2

  loadC2Wall4: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 200;
      object.position.y = 0;
      object.position.z = 70;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },








  loadC2Glass4: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 200;
      object.position.y = 0;
      object.position.z = 70;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC2Grass4: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 200;
      object.position.y = 0;
      object.position.z = 70;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0;//rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },



//C-1


  loadC1Wall5: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 313;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },


  loadC1Glass5: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 313;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC1Grass5: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 313;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },





//C2

  loadC2Wall5: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 313;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },








  loadC2Glass5: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 313;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC2Grass5: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 313;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },

  //C6

loadC1Wall6: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -530;
      object.position.y = 0;
      object.position.z = 310;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },


  loadC1Glass6: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -530;
      object.position.y = 0;
      object.position.z = 310;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC1Grass6: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C1breakdown/C1Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -530;
      object.position.y = 0;
      object.position.z = 310;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },





//C2

  loadC2Wall6: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc,shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -190;
      object.position.y = 0;
      object.position.z = 310;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },








  loadC2Glass6: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, transparent:true, opacity:0.95 ,envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -190;
      object.position.y = 0;
      object.position.z = 310;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },

  loadC2Grass6: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/C2breakdown/C2Grass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -190;
      object.position.y = 0;
      object.position.z = 310;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = - Math.PI/2;//rotattion
      object.scale.set(0.6, 0.7, 0.6);
      lesson6.scene.add(object);

    });
  },




















  //B-1

  loadBFloor: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -180;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.9, 0.9, 0.9);
      lesson6.scene.add(object);

    });
  },





  loadBGrass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BGrass.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0x637341, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -180;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.9, 0.9, 0.9);
      lesson6.scene.add(object);

    });
  },



  loadBTiles: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BTiles.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xeeeeee, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -180;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.9, 0.9, 0.9);
      lesson6.scene.add(object);

    });
  },


  loadBWall: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BWall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xcccccc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -180;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.9, 0.9, 0.9);
      lesson6.scene.add(object);

    });
  },





  

  //LoadBGlass

  loadBGlass: function() {




    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/FBbreakdown/BGlass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -180;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //rotattion
      object.scale.set(0.9, 0.9, 0.9);
      lesson6.scene.add(object);

    });
  },



  //A0


  loadWall: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0.5;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },



//A1

  loadWall1: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -170;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack1: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -170;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor1: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -170;
      object.position.y = 0.5;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass1: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -170;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },



  //A2

  loadWall2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -6;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -6;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -6;
      object.position.y = 0.5;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass2: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -6;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


  //A3

  loadWall3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 164;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 164;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 164;
      object.position.y = 0.5;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass3: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 164;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },




  //A4

  loadWall4: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -200;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack4: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -200;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor4: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -200;
      object.position.y = 0.5;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass4: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -200;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


 //A5

  loadWall5: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -370;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack5: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -370;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor5: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -370;
      object.position.y = 0.5;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass5: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -370;
      object.position.y = 0;
      object.position.z = -250;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI ; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },




  //A6


  loadWall6: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -534;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack6: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -534;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor6: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -534;
      object.position.y = 0.5;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass6: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -534;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },



//A7

  loadWall7: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -364;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack7: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -364;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor7: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -364;
      object.position.y = 0.5;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass7: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -364;
      object.position.y = 0;
      object.position.z = 0;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },




  //A8

  loadWall8: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack8: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor8: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0.5;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass8: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 0;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },



//A9

  loadWall9: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 164;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack9: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 164;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor9: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 164;
      object.position.y = 0.5;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass9: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = 164;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


//A10

  loadWall10: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -6;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack10: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -6;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor10: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -6;
      object.position.y = 0.5;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass10: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -6;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


  //A11

  loadWall11: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -170;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack11: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -170;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor11: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -170;
      object.position.y = 0.5;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass11: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -170;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },





  //A12

  loadWall12: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -534;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack12: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -534;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor12: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -534;
      object.position.y = 0.5;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass12: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -534;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },



//A13

  loadWall13: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -370;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack13: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -370;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor13: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -370;
      object.position.y = 0.5;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass13: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -370;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


//A14

  loadWall14: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -200;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack14: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -200;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor14: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -200;
      object.position.y = 0.5;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass14: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -200;
      object.position.y = 0;
      object.position.z = 50;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = Math.PI; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


  //A15

  loadWall15: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Wall.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshLambertMaterial({ color: 0xdcdcdc, shading: THREE.SmoothShading });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -364;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  //CeilingBlack

  loadCeilingBlack15: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingBlack.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x547bcb, shininess: 5, reflectivity: 0, specular: 0xcf8c18 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -364;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

    //CeilingFloor

  loadCeilingFloor15: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/CeilingFloor.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xdcdcdc });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -364;
      object.position.y = 0.5;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },

  
  //LoadGlass

  loadGlass15: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/A8breakdown/Glass.obj', function(object, materials) {

          var background = new THREE.CubeTextureLoader()
          .setPath( 'textures/pisa/' )
          .load( [ 'pz.jpg', 'nz.jpg', 'py.jpg', 'ny.jpg', 'px.jpg', 'nx.jpg' ] );

      // var material = new THREE.MeshFaceMaterial(materials);
      // var material2 = new THREE.MeshPhongMaterial({color: 0x09172d, transparent: true, opacity: 0.5, specular: 0x050505});
      var material2 = new THREE.MeshBasicMaterial( { color: 0x999999, envMap: background } );

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      //original
      object.position.x = -364;
      object.position.y = 0;
      object.position.z = 300;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(0.6, 0.6, 0.6);
      lesson6.scene.add(object);

    });
  },


  loadModelwater: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/water.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0x7dbeff });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -820;
      object.position.y = 3;
      object.position.z = 460;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(5, 5, 5);
      lesson6.scene.add(object);
    });
  },

  loadModelroad: function() {

    // prepare loader and load the model
    var oLoader = new THREE.OBJLoader();
    oLoader.load('models/road.obj', function(object, materials) {

      // var material = new THREE.MeshFaceMaterial(materials);
      var material2 = new THREE.MeshPhongMaterial({ color: 0xefb378 });

      object.traverse( function(child) {
        if (child instanceof THREE.Mesh) {

          // apply custom material
          child.material = material2;

          // enable casting shadows
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
      
      object.position.x = -820;
      object.position.y = 3;
      object.position.z = 460;
      object.rotation.x = - Math.PI/2;
      object.rotation.z = 0; //Rotattion
      object.scale.set(5, 5, 5);
      lesson6.scene.add(object);
    });
  }
};

function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {

      var textures = [];

      for ( var i = 0; i < tilesNum; i ++ ) {

        textures[ i ] = new THREE.Texture();

      }

      var imageObj = new Image();

      imageObj.onload = function() {

        var canvas, context;
        var tileWidth = imageObj.height;

        for ( var i = 0; i < textures.length; i ++ ) {

          canvas = document.createElement( 'canvas' );
          context = canvas.getContext( '2d' );
          canvas.height = tileWidth;
          canvas.width = tileWidth;
          context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
          textures[ i ].image = canvas
          textures[ i ].needsUpdate = true;

        }

      };

      imageObj.src = atlasImgUrl;

      return textures;

    }

    function onDocumentMouseDown(event) {
        event.preventDefault();
        console.log("get")
        var vector = new THREE.Vector3((event.clientX / window.innerWidth) * 2 -
            1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
        vector.unproject(camera);


        var raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position)
            .normalize());
        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {
           window.open(intersects[0].object.userData.URL, "_self");
        }
      }


// Animate the scene
function animate() {
  requestAnimationFrame(animate);
  render();
  update();
}

// Update controls and stats
function update() {
  lesson6.controls.update(lesson6.clock.getDelta());
}

// Render the scene
function render() {
  if (lesson6.renderer) {
    lesson6.renderer.render(lesson6.scene, lesson6.camera);
  }
}

// Initialize lesson on page load
function initializeLesson() {
  lesson6.init();
  animate();
}

if (window.addEventListener)
  window.addEventListener('load', initializeLesson, false);
else if (window.attachEvent)
  window.attachEvent('onload', initializeLesson);
else window.onload = initializeLesson;
